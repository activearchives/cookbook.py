---
title: LOG
---

24 nov 2016
-------------
Woke up this morning with the thought: this project must be *promiscuous*.

* Are widgets just different kinds of "server flavors" like ?edit and ?serve ?! ... Would be nice to roll the media specific code back into a "media player" HTML ... then make the framing code just deal with hashchange events?!
* Simply drawing on the background could trigger widgets.
* Widgets should "receive" links and have other input/output *nodes* to route messages (instead of the current mess of relaying ALL messages).
* Different kinds of widgets should exist:
    * Viewer (as in iframe, but then better -- ie pdf.js)
* Think how link clicking can map into a TEXT document.
* Widget: Close box
* Editor: hide + extend the options!
* Editor: status: temporary overlay
* Widget: Need font with case (can't check the filenames!)
* Widget: Z-Indexing! (as they overlap!)
* File selection (via icons) + actions (such as delete)... rename with search / replace & patterns (regexp!)
* CRUCIAL: refresh listing without reloading page (and losing widgets!)
* etherpad widget/viewer/editor (or could a similar experience occur somehow directly ?!?!)

Maybe need to make a choice:

* Widgets are all about producing rich editing experiences with interacting components (view + text edit)
* Widgets are about publishing static documents, new ways of writing.
* Or do both of these things work together (rather than need to be split)?

SHOUT OUT TO WEBSTALKER!

WRITING WITH CODE
(promiscuous is a performative writing environment)



23 nov 2016
-----------
* Enabled operation without makefile
* iframe widget
* move fragment code from player to "iframe"(frame) widget


22 nov 2016
---------------

* <s>Rehookup index.cgi as directory listing</s> -- actually this became a proper class (no CGI!)
* <s>Add a frame creator widget (via mini tool palette!)</s> POC complete, now for the real deal
* make sure drag and drop works with this (to enable play & edit!)
* ensure frame messages working
* Eventually remove the cgi-bin!? ... need to transition to HTTP VERBS? (POST for save?)



* 4 Nov 2016: Switch from hard coded directory listing to one that's makefile driven (indexical!)
* 4 Nov 2016: REMOVAL of jinja2 dependency!
* 14 Nov 2016: Change to simplified single makefile
* 18 Nov 2016: Added ?edit syntax and "direct editor" view.

earlier
----------

It seemed very strong how when generalizing the data join, I suddenly (finally) started to care about the form of the incoming data. In other words, the specific labels of the data started to have an effect as they connected (or didn't) with the names of properties in the rdfa "template" element. Thus the document becomes instrumentalized as a sort of filter / query into the incoming data stream. Now, finally, the benefits of data rewriting via a mechanism like json-ld would be most useful (and appreciated) to complete the loop without a need for ad hoc reframing / tweaking of the data. The hit or miss effect of the selection, filtering and ignoring unnecessary data, is reminiscent (in an encouraging way) of json-ld's contexts as filters.

A surprising discovery has been the re-finding of writing "template" code in the form of a sample table row with rdfa properties. It seems it might be useful to revisit early web publishing flows that must have used this kind of mechanism (vague memories of Internet Explorer specific html element properties like data-source).

Next step?
* Data value filters (based on a type? ... or simply connected to a specific value) to do date formating and file size niceness!
 
(later: I realize that this data join stuff was best separate from the main line of the makefile based server and can better be split off and developed on it raises quite a few questions on it's own)

1 Dec 2016
----------------
In explaining the project yesterday, I naturally described the server as the "make server" ... and given that make is now by far the focus of it's operation, this seems indeed the best / most descriptive and honest naming. So I made the change. One interesting consequent is that in typing makeserver to start it, one is reminded, nicedly of the make command. Also: Folded in the submodules including editor (leaving ace-builds a submodule). Need to decide what to do with draggable / player.

TODO: Clean the editor code to not require the (rather) unholy mess of both jquery + d3.

* Enable making files in subdirectories that don't (yet) exist. (make_server.py:FileMaker)


2017
----------

Request.process
	res = site.getResourceFor()
	res.render

NEED FileSaver Resource that gets returned for a file.

TODO

* Separate editor options / cleanup editor
* Use favicon for editor / file type specific ?!
* Wrapping options (without having to resize the browser window)
* Font + font sizes!
* "Sticky" preferences (like theme/fonts/etc)


March 2017
-------------
* add ?touch query option + functionality in main code (remove cgi)

* 12 April 2017: Observation: auto "edit" linking functionality would be nice -- but from the index page (ie automagically adding the ?edit, which can then be manually removed) (ie original decision to do this auto editing INSIDE the server was too rigid)




