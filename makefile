
mdsrc=$(shell ls *.md)
# map *.mp => *.html for mdsrc
html_from_md=$(mdsrc:%.md=%.html)

html: $(html_from_md)

test:
	echo hello world

now:
	touch `date +"%Y-%m-%d-%H%M%S"`.md


%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		$< -o $@

### INDEX FULES ###############
.SECONDEXPANSION:
# holy cow: this is working! (but indexalist is returning the wrong paths (with diversions at start)... need to fix this)
%/index_.html: $$(shell indexalist index --path % --format listing)
	# echo 'prerequisites = $<'
	indexalist index --path $* --group --template extended.html | \
		html5tidy \
			--indent \
			--script /cookbook/draggable/draggable.js \
			--script /cookbook/editor/relaymessages.js \
			--style /cookbook/draggable/draggable.css \
			--script /cookbook/toolpalette.js > $@

index_.html: $(shell indexalist index --path . --format listing)
	indexalist index --path . --group --template extended.html | \
		html5tidy \
			--indent \
			--script /cookbook/draggable/draggable.js \
			--script /cookbook/editor/relaymessages.js \
			--style /cookbook/draggable/draggable.css \
			--script /cookbook/toolpalette.js > $@
###############################

%.html: %.md
	pandoc --standalone $< -o $@

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'

