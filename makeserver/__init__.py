import os

DATAPATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")

def getDataPath (p=None):
    if p:
        return os.path.join(DATAPATH, p)
    else:
        return data
