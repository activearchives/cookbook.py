#!/usr/bin/python
# twisted server
# 1. serve local files from pwd
# 2. serve /lib from aa/htdocs

import argparse, socket, os

from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.static import File, DirectoryLister, getTypeAndEncoding, formatFileSize
from twisted.internet import reactor
from twisted.web.twcgi import CGIDirectory, CGIScript
from twisted.internet.error import CannotListenError
from twisted.python import filepath
# from jinja2 import Environment, PackageLoader, ChoiceLoader, FileSystemLoader
from time import sleep

DIRECTORY_LISTING = None

from cookbook import getDataPath

class StaticDirectoryListingFile (File):
    # def __init__(self, path, *args, **kwargs):
    #     # self.directoryListingResource = directory_listing
    #     super(StaticDirectoryListingFile, self).__init__(path, *args, **kwargs)

    def directoryListing (self):
        return DIRECTORY_LISTING

# DirectoryLister.template = DirectoryLister.template.replace("</style>", """
#     </style>
#     <script src="/cookbook/collagecode.js"></script>
#     <script src="/cookbook/clicktoedit.js"></script>
#     <script src="/cookbook/relaymessages.js"></script>
# """)

def main(args=None):
    parser = argparse.ArgumentParser(description='Happy to serve you')
    parser.add_argument('--port', type=int, default=8000, help='the port number to listen to')
    parser.add_argument('-t', '--notryports', default=True, action="store_false", help='if a port is busy, automatically try other ones')
    parser.add_argument('--share', default=False, action="store_true", help='Run as server accessible via your local network')
    parser.add_argument('--htdocs', default="~/cookbook/", help='')
    parser.add_argument('--cgibin', default="~/cookbook/cgi-bin", help='')
    args = parser.parse_args(args)
    
    tryports = args.notryports
    port = args.port
    ipaddr = None

    if args.share:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("wikipedia.org",80))
        ipaddr = s.getsockname()[0]
        interface = ipaddr # None would listen to all
        s.close()
    else:
        interface = "localhost"

    # cwd = os.getcwd()

    corehtdocspath = getDataPath("htdocs")
    corecgibinpath = getDataPath("cgi-bin")

    # htdocs = os.path.expanduser(args.htdocs)
    # cookbook = File(htdocs)
    # cgibin_path = os.path.expanduser(args.cgibin)
    # cgibin = None
    # if os.path.exists(cgibin_path):
    #    cgibin = CGIDirectory(cgibin_path)
    #    cookbook.putChild("cgi-bin", cgibin)

    listing = CGIScript(os.path.join(corecgibinpath, "makeindex.cgi"))
    global DIRECTORY_LISTING
    DIRECTORY_LISTING = listing
    resource = StaticDirectoryListingFile(".")
    # resource.putChild("cookbook", cookbook)
    corehtdocs = File(corehtdocspath)
    resource.putChild("cookbook", corehtdocs)
    corecgibin = CGIDirectory(corecgibinpath)
    corehtdocs.putChild("cgi-bin", corecgibin)


    factory = Site(resource)

    while True:
        try:
            if ipaddr:
                server_address = (ipaddr, port)
                servername = ipaddr
            else:
                server_address = ("", port)
                servername = "localhost"

            reactor.listenTCP(port, factory, interface=interface)
            print "Archiving starts now --> http://{0}:{1}".format(servername, port)
            reactor.run()

        except (socket.error, CannotListenError) as e:
            if isinstance(e, CannotListenError) or e.errno == 98:
                if tryports:
                    if port < 2000:
                        port = 2000
                    else:
                        port += 1
                    sleep(.01)
                else:
                    print """
====================================
Error: port ({0}) is already in use
====================================

You can pick another port number
(for example 9999) with:

    aa --port 9999
""".format(port)
                    break
            else:
                raise(e)

if __name__ == "__main__":
    main()
