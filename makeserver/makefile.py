import os, subprocess

# def makefile_candidates (path, docroot):
#     """
#     Return a list of (makefiles, cwd) 
#     Considers paths from parent(path) up to and including docroot.
#     And cwd's of 1. makefile, and 2. parent(path) (if different)

#     Example, given files:

#     test/makefile
#     test/foo/makefile
#     test/foo/bar/baz/makefile

#     make_candidates(test/foo/bar/baz/index.html)
#     =>
#     [('test/foo/bar/baz/makefile', 'test/foo/bar/baz'),
#      ('test/foo/makefile', 'test/foo'),
#      ('test/foo/makefile', 'test/foo/bar/baz'),
#      ('test/makefile', 'test'),
#      ('test/makefile', 'test/foo/bar/baz')]

#     """
#     docroot = docroot.rstrip("/")
#     filepath, _ = os.path.split(path)

#     # 1. Consider all directories from that of path up to (and including) docroot
#     allpaths = []
#     p = path
#     while True:
#         p, _ = os.path.split(p)
#         allpaths.append(p)
#         if p == docroot:
#             break
#     # 2. Where to makefiles exist?
#     makefiles = []
#     for p in allpaths:
#         mp = os.path.join(p, "makefile")
#         if os.path.exists(mp):
#             makefiles.append(mp)
#         mp = os.path.join(p, "Makefile")
#         if os.path.exists(mp):
#             makefiles.append(mp)
#     # 3. For each makefile consider performing it in both the path of makefile (first), and in dir of path itself (second)
#     candidates = []
#     for mp in makefiles:
#         p, _ = os.path.split(mp)
#         candidates.append((mp, p))
#         if (p != filepath):
#             candidates.append((mp, filepath))
#     return candidates

def makefile_candidates (path, docroot):
    """
    Returns (makefile, docroot) 
    """
    docroot = docroot.rstrip("/")
    mp = os.path.join(docroot, "makefile")
    if os.path.exists(mp):
        return ((mp, docroot), )
    mp = os.path.join(docroot, "Makefile")
    if os.path.exists(mp):
        return ((mp, docroot), )

def make_question (path, docroot):
    # print ("make_question {0} {1}".format(path, docroot))
    # make --question return codes: 0 : file is up to date, 1 : file needs remaking, 2 : file is not makeable
    for mp, cwd in makefile_candidates(path, docroot):
        # try:
        # print ("make?", path, mp, cwd)
        rpath = os.path.relpath(path, cwd)
        r = subprocess.call(["make", "--question", "-f", mp, rpath], cwd = cwd)        
        # print ("*make_question {0} {1} {2}".format(path, cwd, r))
        if (r == 0 or r == 1):
            return (r, mp, cwd)
        # except subprocss.CalledProcessError as e:
        #     pass
    return (2, None, None)

def make_question_simple (path, makefile, cwd):
    # print ("make_question {0} {1}".format(path, docroot))
    # make --question return codes: 0 : file is up to date, 1 : file needs remaking, 2 : file is not makeable
    rpath = os.path.relpath(path, cwd)
    # print ("*make_question {0} {1} {2}".format(path, cwd, r))
    return subprocess.call(["make", "--question", "-f", makefile, rpath], cwd = cwd)        
