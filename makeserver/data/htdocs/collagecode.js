!function() {
  var cc = {
    version: "0.5.0"
  };
  function extend() {
    for (var i = 1; i < arguments.length; i++) for (var key in arguments[i]) if (arguments[i].hasOwnProperty(key)) arguments[0][key] = arguments[i][key];
    return arguments[0];
  }
  cc.extend = extend;
  function functor(v) {
    return typeof v === "function" ? v : function() {
      return v;
    };
  }
  cc.functor = functor;
  function cells() {
    var that = {}, occupied_cells = [], gridsize = 0, grid = [];
    that.gridsize = function() {
      return gridsize;
    };
    that.grid = grid;
    function resize_grid() {
      for (var y = 0; y < gridsize; y++) {
        var row = grid[y];
        if (row === undefined) {
          row = grid[y] = [];
        }
        for (var x = 0; x < gridsize; x++) {
          var cell = row[x];
          if (cell === undefined) {
            cell = row[x] = {
              x: x,
              y: y,
              elt: null
            };
          }
        }
        row.splice(gridsize);
      }
      grid.splice(gridsize);
    }
    function free_cell() {
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r];
        for (var c = gridsize - 1; c >= 0; c--) {
          var cell = row[c];
          if (cell.elt === null) {
            return cell;
          }
        }
      }
      gridsize += 1;
      resize_grid();
      return free_cell();
    }
    that.add = function(elt) {
      var cell = free_cell();
      cell.elt = elt;
      occupied_cells.push(cell);
      return that;
    };
    function compactable() {
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r], cell = row[gridsize - 1];
        if (cell.elt !== null) {
          return false;
        }
      }
      for (var c = 0; c < gridsize - 1; c++) {
        var cell = row[c];
        if (cell.elt !== null) {
          return false;
        }
      }
      return true;
    }
    function compact() {
      while (gridsize && compactable()) {
        gridsize -= 1;
      }
      resize_grid();
    }
    that.remove = function(elt) {
      for (var i = 0; i < occupied_cells.length; i++) {
        var cell = occupied_cells[i];
        if (cell.elt == elt) {
          cell.elt = null;
          occupied_cells.splice(i, 1);
          compact();
          return that;
        }
      }
      return that;
    };
    function get() {
      if (arguments.length == 0) {
        return occupied_cells.map(function(x) {
          return x.elt;
        });
      } else {
        var i = arguments[0];
        if (i >= 0) {
          return occupied_cells[i].elt;
        } else {
          return occupied_cells[occupied_cells.length + i].elt;
        }
      }
    }
    that.get = get;
    return that;
  }
  function celllayout() {
    var that = {}, grid = [], width = functor(1), height = functor(1), place = null, span = true;
    that.span = function(s) {
      if (arguments.length == 0) {
        return span;
      } else {
        span = s;
        return that;
      }
    };
    that.place = function(p) {
      if (arguments.length == 0) {
        return p;
      } else {
        place = p;
        return that;
      }
    };
    that.width = function(w) {
      if (arguments.length == 0) {
        return width;
      } else {
        width = functor(w);
        return that;
      }
    };
    that.height = function(h) {
      if (arguments.length == 0) {
        return height;
      } else {
        height = functor(h);
        return that;
      }
    };
    function layout(cells) {
      var gridsize = cells.gridsize(), cellwidth = width() / gridsize, cellheight = height() / gridsize;
      for (var r = 0; r < gridsize; r++) {
        var row = cells.grid[r];
        for (var c = 0; c < gridsize; c++) {
          var cell = row[c];
          if (cell.elt !== null) {
            if (!span) {
              place.call(cell.elt, cell.x * cellwidth, cell.y * cellheight, cellwidth, cellheight);
            } else {
              var w = cellwidth, h = cellheight;
              for (var r2 = r + 1; r2 < gridsize; r2++) {
                if (cells.grid[r2][c].elt === null) {
                  h += cellheight;
                } else {
                  break;
                }
              }
              place.call(cell.elt, cell.x * cellwidth, cell.y * cellheight, w, h);
            }
          }
        }
      }
      return that;
    }
    that.layout = layout;
    return that;
  }
  cc.cells = cells;
  cc.celllayout = celllayout;
  function overlay() {
    var that = {}, elt = null, close_callback = null;
    function style(elt, opts) {
      for (var key in opts) {
        elt.style[key] = opts[key];
      }
    }
    function show_item() {
      var refelt = this, itemelt = document.createElement("div"), bg = document.createElement("div"), body = document.createElement("div"), close_button = document.createElement("button"), bcr = refelt.getBoundingClientRect();
      style(itemelt, {
        position: "absolute",
        zIndex: 1,
        left: bcr.x + "px",
        top: bcr.y + "px",
        width: bcr.width + "px",
        height: bcr.height + "px"
      });
      style(bg, {
        position: "absolute",
        left: "0px",
        top: "0px",
        width: "100%",
        height: "100%",
        background: "black",
        opacity: .5,
        zIndex: 1
      });
      style(body, {
        position: "absolute",
        left: "0px",
        top: "0px",
        width: "100%",
        height: "100%",
        textAlign: "center",
        zIndex: 2
      });
      itemelt.appendChild(bg);
      itemelt.appendChild(body);
      body.appendChild(close_button);
      close_button.innerHTML = "close";
      close_button.addEventListener("click", function() {
        if (typeof close_callback == "function") {
          close_callback.call(refelt, refelt);
        }
      });
      elt.appendChild(itemelt);
    }
    function show(items) {
      elt = document.createElement("div");
      that.elt = elt;
      style(elt, {
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%"
      });
      elt.addEventListener("click", function(e) {
        if (e.target == elt) {
          hide();
        }
      });
      for (var i = 0, len = items.length; i < len; i++) {
        show_item.call(items[i]);
      }
    }
    that.show = show;
    function close(callback) {
      close_callback = callback;
      return that;
    }
    that.close = close;
    function hide() {
      elt.remove();
    }
    that.hide = hide;
    return that;
  }
  cc.overlay = overlay;
  cc.ranger = cc_ranger;
  var cc_ranger_defaults = {
    copy: function(x) {
      if (x instanceof Date) {
        var ret = new Date();
        ret.setTime(x.getTime());
        return ret;
      } else {
        return x;
      }
    },
    test: function(start, end, s, e) {
      return s >= start && s < end || e >= start && e < end || s < start && e >= end;
    },
    debug: false
  };
  function cc_ranger(opts) {
    opts = cc_extend({}, cc_ranger_defaults, opts);
    var that = {}, min, max, _start, _end, lastStart, lastEnd, uid = 0, itemsByStart = [], itemsByEnd = [], ssi = -1, sei = -1, esi = -1, eei = -1, activeItems = {};
    that.add = function(elt, start, end, enter, exit) {
      var item = {
        id: "I" + ++uid,
        elt: elt,
        start: start,
        end: end,
        enter: enter,
        exit: exit
      };
      if (start === undefined) {
        return "no start";
      }
      if (end && end < start) {
        return "end is before start";
      }
      if (min === undefined || item.start < min) {
        min = item.start;
      }
      if (max === undefined || item.start > max) {
        max = item.start;
      }
      if (max === undefined || item.end && item.end > max) {
        max = item.end;
      }
      _insert_sorted(item, "start", itemsByStart);
      _insert_sorted(item, "end", itemsByEnd);
      if (opts.test(_start, _end, item.start, item.end)) {
        _enter(item);
        activeItems[item.id] = item;
      } else {
        _exit(item);
      }
      set(_start, _end);
    };
    function _insert_sorted(item, propname, arr) {
      var i = 0, len = arr.length;
      for (;i < len; i++) {
        if (arr[i][propname] > item[propname] || arr[i][propname] === undefined && item[propname] !== undefined) {
          arr.splice(i, 0, item);
          return;
        }
      }
      arr.push(item);
    }
    function _enter(item) {
      if (item.enter) {
        item.enter.call(item.elt);
      } else if (opts.enter) {
        opts.enter.call(item.elt);
      }
    }
    function _exit(item) {
      if (item.exit) {
        item.exit.apply(item.elt);
      } else if (opts.exit) {
        opts.exit.apply(item.elt);
      }
    }
    function nextInterestingTime() {
      var next_start = esi + 1 < itemsByStart.length ? itemsByStart[esi + 1] : undefined, next_end = eei + 1 < itemsByEnd.length ? itemsByEnd[eei + 1] : undefined, ret;
      if (next_start) {
        console.log("next_start", next_start.start, next_start.elt);
        ret = next_start.start;
      }
      if (next_end) {
        console.log("next_end", next_end.end, next_end.elt);
        ret = Math.min(ret, next_end.end);
      }
      return ret;
    }
    that.nextInterestingTime = nextInterestingTime;
    function prevInterestingTime() {
      var prev_start = ssi >= 0 && ssi < itemsByStart.length ? itemsByStart[ssi] : undefined, ret;
      if (prev_start) {
        console.log("prev_start", prev_start.start, prev_start.elt);
        ret = prev_start.start;
      }
      return ret;
    }
    that.prevInterestingTime = prevInterestingTime;
    function updateForValue(start, end) {
      if (itemsByStart.length === 0) {
        return;
      }
      var toUpdate = {}, n;
      function mark(item) {
        toUpdate[item.id] = item;
      }
      if (start >= lastStart) {
        n = itemsByStart.length;
        while (ssi + 1 < n && start >= itemsByStart[ssi + 1].start) {
          ssi++;
          if (ssi < n) {
            mark(itemsByStart[ssi]);
          }
        }
        n = itemsByEnd.length;
        while (sei + 1 < n && start >= itemsByEnd[sei + 1].end) {
          sei++;
          if (sei < n) {
            mark(itemsByEnd[sei]);
          }
        }
      } else {
        while (ssi >= 0 && start < itemsByStart[ssi].start) {
          mark(itemsByStart[ssi]);
          ssi--;
        }
        while (sei >= 0 && start < itemsByEnd[sei].end) {
          mark(itemsByEnd[sei]);
          sei--;
        }
      }
      lastStart = opts.copy(start);
      if (end >= lastEnd) {
        n = itemsByStart.length;
        while (esi + 1 < n && end > itemsByStart[esi + 1].start) {
          esi++;
          if (esi < n) {
            mark(itemsByStart[esi]);
          }
        }
        n = itemsByEnd.length;
        while (eei + 1 < n && end >= itemsByEnd[eei + 1].end) {
          eei++;
          if (eei < n) {
            mark(itemsByEnd[eei]);
          }
        }
      } else {
        while (esi >= 0 && end < itemsByStart[esi].start) {
          mark(itemsByStart[esi]);
          esi--;
        }
        while (eei >= 0 && end <= itemsByEnd[eei].end) {
          mark(itemsByEnd[eei]);
          eei--;
        }
      }
      lastEnd = opts.copy(end);
      var item_id;
      for (item_id in toUpdate) {
        if (toUpdate.hasOwnProperty(item_id)) {
          var item = toUpdate[item_id], was_in_range = activeItems[item_id] !== undefined, now_in_range = opts.test(_start, _end, item.start, item.end);
          if (was_in_range != now_in_range) {
            if (now_in_range) {
              activeItems[item_id] = item;
              _enter(item);
            } else {
              delete activeItems[item_id];
              _exit(item);
            }
          }
        }
      }
    }
    function debug() {
      var ret = "";
      ret += "RANGER: " + _start + ", " + _end + "\n";
      ret += "ITEMSBYSTART:\n";
      for (var i = 0; i < itemsByStart.length && i < 10; i++) {
        var item = itemsByStart[i];
        if (ssi == i) {
          ret += "S";
        }
        if (esi == i) {
          ret += "E";
        }
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      ret += "ITEMSBYEND:\n";
      for (var i = 0; i < itemsByEnd.length && i < 10; i++) {
        var item = itemsByEnd[i];
        if (sei == i) {
          ret += "S";
        }
        if (eei == i) {
          ret += "E";
        }
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      var actives = getActive();
      ret += "ACTIVES: " + actives.length;
      console.log(ret);
    }
    function set(start, end) {
      _start = start;
      _end = end;
      updateForValue(start, end);
      if (opts.debug) {
        debug();
      }
    }
    function getActive() {
      var ret = [];
      for (var item_id in activeItems) {
        if (activeItems.hasOwnProperty(item_id)) {
          ret.push(activeItems[item_id]);
        }
      }
      return ret;
    }
    that.getActive = getActive;
    that.debug = function(val) {
      opts.debug = val;
    };
    that.set = set;
    that.get = function() {
      return currentValue;
    };
    that.min = function() {
      return min;
    };
    that.max = function() {
      return max;
    };
    return that;
  }
  cc.xhr = cc_xhr;
  function cc_xhr(url, responseType, callback) {
    var httpRequest;
    if (window.XMLHttpRequest) {
      httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      try {
        httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
      }
    }
    httpRequest.onreadystatechange = function() {
      var response;
      if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
          if (responseType == "xml") {
            response = httpRequest.responseXML;
          } else if (responseType == "json") {
            response = JSON.parse(httpRequest.responseText);
          } else {
            response = httpRequest.responseText;
          }
          callback.call(httpRequest, response);
        } else {}
      } else {}
    };
    httpRequest.open("GET", url, true);
    httpRequest.send(null);
    return httpRequest;
  }
  cc.json = cc_json;
  function cc_json(url, callback) {
    return cc_xhr(url, "json", callback);
  }
  function uuid() {
    var d = new Date().getTime();
    var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == "x" ? r : r & 3 | 8).toString(16);
    });
    return uuid;
  }
  cc.uuid = uuid;
  cc.virtualcanvas = cc_virtualcanvas;
  function cc_virtualcanvas(opts) {
    var that = {}, zoom = 1, items = [];
    function update(item) {
      opts.zoom.call(item.elt, item.x / zoom, item.y / zoom, item.w / zoom, item.h / zoom);
    }
    that.zoom = function(z, sx, sy) {
      if (z === undefined) {
        return zoom;
      } else {
        if (z !== zoom) {
          zoom = z;
          for (var i = 0, l = items.length; i < l; i++) {
            var item = items[i];
            update(item);
          }
        }
        return that;
      }
    };
    that.place = function(elt, x, y, w, h) {
      var item = {
        elt: elt,
        x: x,
        y: y,
        w: w,
        h: h
      };
      items.push(item);
      update(item);
      return that;
    };
    return that;
  }
  function window_width() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerWidth || e.clientWidth || g.clientWidth;
  }
  function window_height() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerHeight || e.clientHeight || g.clientHeight;
  }
  cc.window_width = window_width;
  cc.window_height = window_height;
  if (typeof define === "function" && define.amd) define(cc); else if (typeof module === "object" && module.exports) module.exports = cc;
  this.cc = cc;
}();