document.addEventListener("DOMContentLoaded", function () {


	/**********************************/
	/* TIMECODE */
	function aa_secondsToTimecode (ss, style) {
	    style = timecode_styles[style || 'html5'];
	    var h = Math.floor(ss/3600),
	        m,
	        s,
	        fract,
	        ret;
	    ss -= (h * 3600);
	    m = Math.floor(ss / 60);
	    ss -= (m * 60);
	    s = Math.floor(ss);
	    fract = style.delimiter + (""+(ss - s)).substr(2, 3);
	    while (fract.length < 4) { fract += "0"; } // PAD TO ALWAYS BE THREE DIGITS LONG (eg .040)
	    ret = (h || style.requireHours ? ((((h<10)?"0":"")+h)+":") : '')
	    ret += (((m<10)?"0":"")+m)+":"+(((s<10)?"0":"")+s)+fract;
	    return ret;
	}

	timecode_styles = {
	    'srt': {
	        requireHours: true,
	        delimiter: ','
	    },
	    'html5': {
	        requireHours: false,
	        delimiter: '.'
	    }
	}

	function aa_timecodeToSeconds (tc) {
	    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/,
	        groups = tcpat.exec(tc);
	    if (groups != null) {
	        var h = groups[1] !== undefined ? parseInt(groups[1]) : 0,
	            m = parseInt(groups[2]),
	            s = parseFloat(groups[3].replace(/,/, "."));
	        return (h * 3600) + (m * 60) + s;
	    }
	}
	/**********************************/

	var allwins = [];
	function update_wins (w) {
		var need_to_add = true;
		// console.log("update_wins", w);
		for (var i=0, l=allwins.length; i<l; i++) {
			if (allwins[i] === w) {
				// console.log(i, "known", w);
				need_to_add = false;
				// return;
				/// WEIRD: so even though the window object appears to be the same...
				// it seems you need to RESET the message listener event on a LOAD ?!
			}
		}
		if (need_to_add) {
			w.name = guid();
			allwins.push(w);
			// console.log("added", w.name, allwins.length);

		}
		// watch for cb messages and repost to others...
		// console.log("addEventListener.message", w.name);	
		w.addEventListener("message", function (e) {
			// console.log("RECV", e);
			if (e.data && e.data.relay) {
				// console.log("refusing to relay an already relayed message");
				return;
			}
			for (var i=0, len=allwins.length; i<len; i++) {
				var w2 = allwins[i];
				if (e.data && e.data.origin && e.data.origin != w2.name) {
					// console.log("relaying message", e.data, "from", e.data.origin, "to", w2.name);
					e.data.relay = true;
					w2.postMessage(e.data, "*");
				} else {
					// console.log("norelay", e.data.origin, " ", w2.name);;
				}
			}
		})
	}
	function relaymessage (w, msg) {
		for (var i=0, len=allwins.length; i<len; i++) {
			var w2 = allwins[i];
			if (w2 !== w) { w2.postMessage(msg, "*"); }
		}
	}

	function droppable (elt, cb) {
		elt.ondragover = elt.ondragenter = function(event) {
		    event.stopPropagation();
		    event.preventDefault();
		}
		elt.ondrop = function(event) {
		    event.stopPropagation();
		    event.preventDefault();
		    cb.call(elt, event);
		}		
	}

	function make_widget (src) {
		var elt = document.createElement("div"),
			body = document.createElement("div"),
			iframe = document.createElement("iframe"),
			urldiv = document.createElement("div"),
			url = document.createElement("input");

		urldiv.style.position = "absolute";
		urldiv.style.background = "pink";
		urldiv.style.left = "10px";
		urldiv.style.right = "10px";
		urldiv.style.top = "5px";

		url.setAttribute("type", "text");
		url.classList.add("url");
		url.style.background = "none";
		url.style.color = "white";
		url.style.fontSize = "10px";
		
		urldiv.appendChild(url);
		elt.appendChild(urldiv);
		url.style.position = "absolute";
		url.style.left = "0";
		url.style.top = "0"
		url.style.width = "100%"
		url.style.border = "none";

		elt.appendChild(body);
		body.classList.add("body");
		elt.style.position = "fixed";
		elt.style.right = "5px";
		elt.style.top = "5px";
		elt.style.width = "300px";
		elt.style.height = "300px";
		elt.style.background = "black";
		elt.style.cursor = "move";
		body.style.position = "absolute";
		body.style.left = "10px";
		body.style.top = "22px";
		body.style.right = "10px";
		body.style.bottom = "10px";
		body.style.background = "white";
		iframe.style.position = "absolute";
		iframe.style.left = "0px";
		iframe.style.top = "0px";
		iframe.style.width = "100%";
		iframe.style.height = "100%";
		iframe.style.border = "none";
		body.appendChild(iframe);

		// EVENTS
		iframe.addEventListener("load", function () {
			var fw = iframe.contentWindow,
				hashWillChange = false,
				media;

			update_wins(fw);

			// console.log("iframe load");
			// nb: the contentWindow is new each load (so no problem to do this onload)
			fw.addEventListener("hashchange", function () {
				if (hashWillChange) {
					hashWillChange = false;
				} else {
					console.log("(external) HASHCHANGE", this.location.hash);
					if (media) {
						var m = this.location.hash.match(/#t=(.+)/);
						if (m != null) {
							var t = aa_timecodeToSeconds(m[1]);
							console.log("setting media time to", t, m[1]);
							media.currentTime = t;
						}
					}
				}
			})

			fw.addEventListener("message", function (e) {	
				if (e.data.msg == "cc_toggle") {
					if (media) {
						media.paused ? media.play() : media.pause();
					}
				} else if (e.data.msg == "cc_left") {
					if (media) {
						media.currentTime = Math.max(0, media.currentTime - 5);
					}
				} else if (e.data.msg == "cc_right") {
					if (media) {
						media.currentTime = media.currentTime + 5;
					}
				} else if (e.data.msg == "fragmentclick") {
					// console.log("receiving framgmentclick", e.data);
					if (e.data.startNormalized && media) {
						media.currentTime = e.data.startNormalized;
					}
				}
			}, false);

			var mediaelts = this.contentDocument.querySelectorAll("audio,video");
			if (mediaelts.length) {
				// console.log("media detected");
				for (var i=0, len = mediaelts.length; i<len; i++) {
					media = mediaelts[i];
					media.addEventListener("timeupdate", function () {
						// console.log("timeupdate", this.currentTime);
					    var start = this.currentTime,
					        start_tc = aa_secondsToTimecode(start),
					       	new_hash = "#t="+start_tc;

					    // console.log("tc", start_tc);
						// force a hashchange in response to media's timeupdate					    
					    if (fw.location.hash != new_hash) {
					    	hashWillChange = true;
						    fw.location.hash = new_hash;
					    }
					    var parts = url.value.split("#");
				    	url.value = parts[0] + new_hash;

				    	// console.log("postMessage", fw.location.pathname, fw.location.hash, start_tc, start);
					    relaymessage(fw, {
					        msg: "fragmentchange",
					        origin: fw.name,
					        href: fw.location.pathname,
					        value: fw.location.pathname+fw.location.hash,
					        hash: {
					            t: [{
					                start: start_tc,
					                startNormalized: start,
					                unit: "npt",
					                value: start_tc,
					                end: "",
					                endNormalized: ""
					            }]
					        }
					    });
					}, false);
				}
			}
		})



		iframe.src = src;
		document.body.appendChild(elt);
		draggable.edges(elt);

		url.addEventListener("change", function () {
			iframe.src = this.value;
		});

		droppable(elt, function (event) {
		    var text = event.dataTransfer.getData("text");
		        // urilist = event.dataTransfer.getData("text/uri-list"),
		        // html = event.dataTransfer.getData('text/html');
		    iframe.src = text;
		    // strip the server
		    var pat = /https?:\/\/([a-z_\-\.]+(?:\:\d+))\/(.+)$/,
		    	m = pat.exec(text),
		    	label = text;
		    // console.log("m", m, window.location.host);
		    if (m && m[1] == window.location.host) {
		    	label = "/" + m[2];
		    }
		    url.value = label;
		});


		return elt;
	}

		// console.log("loaded");
	document.addEventListener("mousedown", function (e) {
		// console.log("mousedown", e);
		if (e.target == document.body && e.ctrlKey) {
			make_widget("");
		}
	}, false);

	// RELAYMESSAGES
	// http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
	function guid() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	    s4() + '-' + s4() + s4() + s4();
	}

	/*
	function relaymessages () {
		// Array.prototype.slice.call(
		var uuid = guid(),
			attr = "data-relaymessages-"+uuid;
		console.log("relaymessages", uuid)

		function relay_iframe_messages_to_others (f) {
			// console.log("WATCHING", f, f.contentWindow);
			// if (f.contentDocument.readyState === "complete") { iframe_on_load.call(f) }
			// f.addEventListener("load", iframe_on_load, false);
			f.contentWindow.addEventListener("message", function (e) {
				console.log("RECV", e, "source", e.source, "cwindow", f.contentWindow);
				// only propagate messages originating from the frame itself (and thus not re-propagated messages)
				var iframes = document.querySelectorAll("iframe");
				if (e.source == f.contentWindow) {
					// console.log("RECV2", e, iframes.length);
					for (var i=0, l=iframes.length; i<l; i++) {
						var f2 = iframes[i];
						// don't propate to yourself
						// if (e.source !== f2.contentWindow) { // should be the same as below
						if (f !== f2) {
							// console.log("relaying message from", f, "to", f2);
							f2.contentWindow.postMessage(e.data, "*");
						} else {
							console.log("skipping self")
						}
					}
				}
			});
		}

		window.setInterval(function () {
			var iframes = document.querySelectorAll("iframe");
			for (var i=0,l=iframes.length; i<l; i++) {
				if (!iframes[i].getAttribute(attr)) {
					iframes[i].setAttribute(attr, "1");
					console.log("relaying messages from new iframe", iframes[i]);
					relay_iframe_messages_to_others(iframes[i]);
				}
			}
			// console.log("relaymessages", iframes.length);
		}, 2500);
		return;
		// event bridge
	}
	*/
	// relaymessages();

}, false);

