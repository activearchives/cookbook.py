(function () {
	// goal: parsing returns a list of json like objects
	// where object attributes remain DOM-linked
	// So that editing the data structure directly modifies the DOM
	// CF: https://www.w3.org/TR/rdfa-core/

	// Value: keep the DOM the primary item (in spirit of jquery)

	// process space delimited strings in @property
	// about & resource
	// 2016-10-16: make join work for reloading a page

	function situated_data () {
		var that = {},
			opts = {
				exitAnonymousFalse: true,
				updateFalsyValues: false // true means falsy value *will* get updated / set on an update, false means they are skipped (preserving page values)
			};

		that.parse = function (node) {
			that.data = situated_data_parser(node);
			return that;
		}

		that.join = function (data, defaultItemType) {
			// TODO: Generalize, remove d3 deps
			// create item index

			// I. scan existing items to build: id index, get the template item (first matching type), and lastNode (last matching type)

			var dataById = {},
				itemTemplate, lastNode,
				idCount = 0;

			// seek template


			for (var i=0, l=that.data.length; i<l; i++) {
				var d = that.data[i];
				if (d.id) {
					dataById[d.id] = d;
					idCount++;
				}
				if (d.type == defaultItemType) {
					if (d.__node__.classList.contains('template')) {
						itemTemplate = d;
					}
					// if (!itemTemplate) { itemTemplate = d; }
					lastNode = d.__node__;
				}
			}
			console.log("join I. ids: " + idCount + ", itemTemplate: " + itemTemplate + ", lastNode: " + lastNode);

			// II. scan incoming data to determine enter/update/exit sets

			var enter = [],
				update = [],
				in_update = {},
				exit = [];

			for (var i=0, l=data.length; i<l; i++) {
				var d = data[i];
				if (d.id && dataById[d.id]) {
					update.push({
						incoming: d,
						page: dataById[d.id]
					});
					in_update[d.id] = true;
				} else {
					enter.push(d);
				}
			}
			// calc exit set as diff of existing with in_update
			for (var i=0, l=that.data.length; i<l; i++) {
				// what to do with anonymous data, could either exit it or keep it...
				var d = that.data[i];
				// NB: only considers elements of the given type
				if (d.type == defaultItemType) {
					if (!d.id) {
						if (opts.exitAnonymousData) { exit.push(d); }
					} else {
						if (!in_update[d.id]) { exit.push(d) }
					}
				}
			}
			console.log("join II. " + enter.length + " enter / " + update.length + " update / " + exit.length + " exit");
			console.log("enter", enter);
			console.log("update", update);
			console.log("exit", exit);

			// III. a. ENTER

			for (var i=0, l=enter.length; i<l; i++) {
				var d = enter[i],
					newNode = itemTemplate.__node__.cloneNode(true);
				newNode.classList.remove("template");
				// itemTemplate.__node__.parentNode.appendChild(newNode);
				itemTemplate.__node__.parentNode.insertBefore(newNode, lastNode.nextSibling);
				lastNode = newNode;
				var nd = situated_data_parser(newNode)[1];
				// console.log("nd", nd);
				// nd.filename = d.filename;
				for (var x in d) {
					// console.log("transfering property", x);
					nd[x] = d[x];
					// assign_property_to(nd, d, x);
				}
				if (d.id) {
					newNode.setAttribute("resource", d.id);
				}
			}

			/// III. b. UPDATE
			for (var i=0, l=update.length; i<l; i++) {
				var di = update[i].incoming,
					dp = update[i].page;
				for (var x in di) {
					// console.log("transfering property", x);
					if (opts.updateFalsyValues || di[x]) {
						dp[x] = di[x];
						// assign_property_to(dp, di, x);
					}
				}
			}

			/// III. c. EXIT
			for (var i=0, l=exit.length; i<l; i++) {
				var d = exit[i];
				d.__node__.parentNode.removeChild(d.__node__);
			}

		}


		return that;
	}

	function humanFileSize(bytes, si) {
	    var thresh = si ? 1000 : 1024;
	    if(Math.abs(bytes) < thresh) {
	        return bytes + ' B';
	    }
	    var units = si
	        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
	        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
	    var u = -1;
	    do {
	        bytes /= thresh;
	        ++u;
	    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
	    return bytes.toFixed(1)+' '+units[u];
	}

	function pad(x, places, char) {
		x = "" + x;
		char = char || "0";
		while(x.length < places) {
			x = char+x;
		}
		return x;
	}

	function strftime (d) {
		return d.getFullYear()+"-"+pad(d.getMonth()+1, 2, "0")+"-"+pad(d.getDate(), 2, "0")+" "+pad(d.getHours(), 2, "0")+":"+pad(d.getMinutes(), 2, "0");
	}

	function set_node_value (node, value, pname) {
		if (pname == "contentSize") {
			node.setAttribute("content", value);
			node.innerHTML = humanFileSize(value);
		} else if (node.nodeName == "TIME") {
			node.setAttribute("datetime", value);
			var t = new Date(value);
			// node.innerHTML = t.toLocaleDateString();
			node.innerHTML = strftime(t);
		} else {
			node.innerHTML = value;
		}
	}


	function situated_data_parser (node, cur_object, all_objects) {
		if (node == undefined) {
			node = document;
		}
		if (all_objects == undefined) {
			all_objects = [];
		}
		if (cur_object == undefined) {
			cur_object = {};
			all_objects.push(cur_object);
		}
		if (node.getAttribute) {
			if (node.getAttribute("typeof")) {
				cur_object = {__node__: node}
				all_objects.push(cur_object);
				cur_object.type = node.getAttribute("typeof");
			}
			if (node.getAttribute("resource") || node.getAttribute("about")) {
				cur_object.id = node.getAttribute("resource") || node.getAttribute("about");
			}
			if (node.getAttribute("property")) {
				var propertyName = node.getAttribute("property");
				// todo: how to deal with multiple values (currently attempting to redefine the property will fail)
				// ideally, the getter/setters could deal with multiplicity ... but this requires a means
				// of defining arity of properties ... FOR NOW: simplicity of singular, setter = REPLACE VALUE
				if (node.nodeName == "IMG") {
					Object.defineProperty(cur_object, propertyName, {
						get: function () { return node.getAttribute("src") },
						set: function (value) { node.setAttribute("src", value) }
					});				
				} else if (node.nodeName == "A") {
					Object.defineProperty(cur_object, propertyName, {
						get: function () { return node.getAttribute("href") },
						set: function (value) { node.setAttribute("href", value) }
					});				
				} else {
					Object.defineProperty(cur_object, propertyName, {
						get: function () { return node.innerHTML; },
						// set: function (value) { node.innerHTML = value }
						set: function (value) {
							set_node_value(node, value, propertyName);
						}
					});				
				}
				Object.defineProperty(cur_object, propertyName + "__node__", {
					get: function () { return node }
				})
			}
		}
		for (var i=0, l=node.childNodes.length; i<l; i++) {
			var childNode = node.childNodes[i];
			situated_data_parser(childNode, cur_object, all_objects);
		}
		return all_objects;
	}

	window.get_situated_data = function () {
		return situated_data().parse(document);
	}

})();