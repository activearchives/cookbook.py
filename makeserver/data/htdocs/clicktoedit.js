document.addEventListener("DOMContentLoaded", function () {
	// console.log("document", document, document.body);
	document.body.style.overflow = "hidden";
	var wrap = document.createElement("div");
	wrap.style.position = "absolute";
	wrap.style.left = "0";
	wrap.style.top = "0";
	wrap.style.width = "100%";
	wrap.style.height = "100%";
	wrap.style.overflow = "auto";
	wrap.classList.add("wrap");
	while (document.body.childNodes.length > 0) {
		wrap.appendChild(document.body.childNodes[0]);
	}
	document.body.appendChild(wrap);
	cells.add(wrap);
	contents = wrap;
	return wrap;
})


var cells = cc.cells(),
	layout = cc.celllayout()
		.width(cc.window_width)
		.height(cc.window_height)
		.place(function (x, y, w, h) {
			// console.log("place", this, x, y, w, h);
			this.style.left = x+"px";
			this.style.top = y+"px";
			this.style.width = w+"px";
			this.style.height = h+"px";
		}),
	overlay = cc.overlay()
		.close(function (elt) {
			// console.log("close", elt);
			cells.remove(elt);
			elt.remove()
			layout.layout(cells);
			overlay.hide();
		});

var edit = document.getElementById("edit");
if (edit) {
	edit.addEventListener("click", function (e) {
		var items = cells.get();
		items.shift();
		overlay.show(items);
		document.body.appendChild(overlay.elt);
	});

}

window.addEventListener("resize", function () {
	layout.layout(cells);
});

var editor = null,
	player = null;

document.addEventListener("click", function (e) {
	// console.log(e);
	if (e.target.nodeName == "A") {
		e.preventDefault();
		var href = e.target.href;
		console.log("href", href);
		if (href.search(/\.(mp4|mp3|m4a|m4v|webm|ogg)$/) !== -1) {
			var relhref = e.target.getAttribute("href");
			console.log("video", relhref);
			if (player == null) {
				player = document.createElement("div");
				document.body.appendChild(player);
				player.style.position = "absolute";
				var f = document.createElement("iframe");
				f.style.width = "100%";
				f.style.height = "100%";
				f.style.border = "none";

				player.appendChild(f);
				cells.add(player);
				layout.layout(cells);
			}
			player.querySelector("iframe").src = "/cookbook/video.html#href="+relhref;

		// } if (href.search(/\.(srt|json|html|js|py|txt|md|css)$/) !== -1) {
		} else if (href.search(/\/$/) == -1) {
			var relhref = e.target.getAttribute("href");
			console.log("text", relhref);
			if (editor == null) {
				editor = document.createElement("div");
				document.body.appendChild(editor);
				editor.style.position = "absolute";
				var f = document.createElement("iframe");
				f.style.width = "100%";
				f.style.height = "100%";
				f.style.border = "none";
				editor.appendChild(f);
				cells.add(editor);
				layout.layout(cells);
			}
			editor.querySelector("iframe").src = "/cookbook/editor.html#href="+relhref;
			
		}

	}
}, false);