(function () {
	function make_widget (src) {
		var elt = document.createElement("div"),
			body = document.createElement("div"),
			iframe = document.createElement("iframe");
		elt.appendChild(body);
		body.classList.add("body");
		elt.style.position = "fixed";
		elt.style.left = "10px";
		elt.style.top = "10px";
		elt.style.width = "300px";
		elt.style.height = "300px";
		body.style.position = "absolute";
		body.style.left = "20px";
		body.style.top = "20px";
		body.style.right = "20px";
		body.style.bottom = "20px";
		iframe.style.position = "absolute";
		iframe.style.left = "0px";
		iframe.style.top = "0px";
		iframe.style.width = "100%";
		iframe.style.height = "100%";
		iframe.style.border = "none";
		body.appendChild(iframe);
		iframe.src = src;
		document.body.appendChild(elt);
		draggable.edges(elt);
		return elt;
	}

	document.addEventListener("DOMContentLoaded", function () {
		var tp = document.querySelector("#aatoolpalette");
		if (!tp) {
			tp = document.createElement("div");
			tp.setAttribute("id", "aatoolpalette");
			document.body.appendChild(tp);
		}
		tp.style.position = "fixed";
		tp.style.right = "10px";
		tp.style.top = "10px";
		// tp.innerHTML = "TOOLPALETTE";
		tp.style.background = "pink"

		var b = tp.querySelector(".editorbutton");
		if (!b) {
			b = document.createElement("button");
			tp.appendChild(b);
		}
		b.innerHTML = "editor";
		b.addEventListener("click", function () {
			make_widget("/cookbook/editor/editor.html");
		});

		b = tp.querySelector(".playerbutton");
		if (!b) {
			b = document.createElement("button");
			tp.appendChild(b);
		}
		b.innerHTML = "player";
		b.addEventListener("click", function () {
			make_widget("/cookbook/player/player.html");
		});

	})

})();