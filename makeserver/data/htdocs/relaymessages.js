(function () {

function relaymessages () {
	// Array.prototype.slice.call(
	var uuid = cc.uuid(),
		attr = "data-relaymessages-"+uuid;
	console.log("relaymessages", uuid)

	function relay_iframe_messages_to_others (f) {
		f.contentWindow.addEventListener("message", function (e) {
			// only propagate messages originating from the frame itself (and thus not re-propagated messages)
			var iframes = document.querySelectorAll("iframe");
			if (e.source == f.contentWindow) {
				for (var i=0, l=iframes.length; i<l; i++) {
					var f2 = iframes[i];
					// don't propate to yourself
					// if (e.source !== f2.contentWindow) { // should be the same as below
					if (f !== f2) {
						// console.log("relaying message from", f, "to", f2);
						f2.contentWindow.postMessage(e.data, "*");
					}
				}
			}
		});
	}

	window.setInterval(function () {
		var iframes = document.querySelectorAll("iframe");
		for (var i=0,l=iframes.length; i<l; i++) {
			if (!iframes[i].getAttribute(attr)) {
				iframes[i].setAttribute(attr, "1");
				console.log("relaying messages from new iframe", iframes[i]);
				relay_iframe_messages_to_others(iframes[i]);
			}
		}
		// console.log("relaymessages", iframes.length);
	}, 2500);
	return;
	// event bridge
}

window.relaymessages = relaymessages;
})();

