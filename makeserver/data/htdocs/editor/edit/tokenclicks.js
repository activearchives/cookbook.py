(function (ace) {
    var define = ace.define;


define("kitchen-sink/token_clicks",["require","exports","module","ace/lib/dom","ace/lib/oop","ace/lib/event","ace/range","ace/tooltip"], function(require, exports, module) {
"use strict";

var dom = require("ace/lib/dom");
var oop = require("ace/lib/oop");
var event = require("ace/lib/event");
var Range = require("ace/range").Range;
// var Tooltip = require("ace/tooltip").Tooltip;
var EventEmitter = require("ace/lib/event_emitter").EventEmitter;
var Selection = require("ace/selection").Selection;

/* See http://jsbin.com/jehopaja/4/edit */

function TokenClicks (editor, opts) {
    if (editor.tokenClicks)
        return;

    editor.tokenClicks = this;
    this.editor = editor;

    this.update = this.update.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this.onClick = this.onClick.bind(this);
    event.addListener(editor.renderer.scroller, "mousemove", this.onMouseMove);
    event.addListener(editor.renderer.content, "mouseout", this.onMouseOut);
    event.addListener(editor.renderer.content, "click", this.onClick);
    this.onTokenChange = opts.tokenChange.bind(this);
}

(function() {
    oop.implement(this, EventEmitter);
    this.token = {};
    this.marker = null; // new Range();

    this.update = function() {
        this.$timer = null;
        var editor = this.editor;
        var renderer = editor.renderer;
        
        var canvasPos = renderer.scroller.getBoundingClientRect();
        var offset = (this.x + renderer.scrollLeft - canvasPos.left - renderer.$padding) / renderer.characterWidth;
        var row = Math.floor((this.y + renderer.scrollTop - canvasPos.top) / renderer.lineHeight);
        var col = Math.round(offset);

        var screenPos = {row: row, column: col, side: offset - col > 0 ? 1 : -1};
        var session = editor.session;
        var docPos = session.screenToDocumentPosition(screenPos.row, screenPos.column);
        
        var selectionRange = editor.selection.getRange();
        if (!selectionRange.isEmpty()) {
            if (selectionRange.start.row <= row && selectionRange.end.row >= row)
                return this.clear();
        }
        
        var line = editor.session.getLine(docPos.row);
        if (docPos.column == line.length) {
            var clippedPos = editor.session.documentToScreenPosition(docPos.row, docPos.column);
            if (clippedPos.column != screenPos.column) {
                return this.clear();
            }
        }
        var token = session.getTokenAt(docPos.row, docPos.column);
        if (token != this.token) {
            this.token = token;
            if (!token) {
                return this.clear();
            }
            token.row = docPos.row;
            var clickable = this.onTokenChange(this.token);
            // console.log("token", token, clickable);          
            if (clickable) {
                // console.log("token", token.row, token.start, token.value);
                // editor.renderer.setCursorStyle("pointer");
                session.removeMarker(this.marker);
                this.isOpen = true
                this.range =  new Range(token.row, token.start, token.row, token.start + token.value.length);
                this.marker = session.addMarker(this.range, "ace_link_marker", "text", false);
            } else {
                this.clear();
            }
        }
        
    };

    this.clear = function() {
        if (this.isOpen) {
            this.editor.session.removeMarker(this.marker);
            this.editor.renderer.setCursorStyle("");
            this.isOpen = false;
        }
    };

    this.onMouseMove = function(e) {
        if (this.editor.$mouseHandler.isMousePressed) {
            if (!this.editor.selection.isEmpty())
                this.clear();
            return;
        }
        this.x = e.clientX;
        this.y = e.clientY;
        this.update();
    };

    this.onMouseOut = function(e) {
        // console.log("mouseOut");
        // this.clear();
    };

    this.onClick = function (e) {
        // console.log("click", e, this.token);
        if (this.token) {
            // this.token.editor = this.editor;
            this._signal("tokenclick", this.token);
            // this.clear()
        }        
    }

    this.destroy = function() {
        this.onMouseOut();
        event.removeListener(this.editor.renderer.scroller, "mousemove", this.onMouseMove);
        event.removeListener(this.editor.renderer.content, "mouseout", this.onMouseOut);
        event.removeListener(this.editor.renderer.content, "click", this.onClick);
        delete this.editor.tokenTooltip;
    };

}).call(TokenClicks.prototype);

exports.TokenClicks = TokenClicks;

});


})(ace);
