(function () {

function post (url, data, callback) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    // request.setRequestHeader('Content-Type', 'application/octet-stream');
    request.onload = function() {
      if (request.readyState == XMLHttpRequest.DONE && request.status >= 200 && request.status < 400) {
        // Success!
        var resp = request.responseText;
        callback(null, resp);
      } else {
        callback("server error");
      }
    };
    request.onerror = function() {
        callback("connection error");
    };
    var sdata = "";
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            sdata += (sdata?"&":"") + key+"="+encodeURIComponent(data[key]);
        }
    }
    request.send(sdata);            
}

function aa_secondsToTimecode (ss, style) {
    style = timecode_styles[style || 'html5'];
    var h = Math.floor(ss/3600),
        m,
        s,
        fract,
        ret;
    ss -= (h * 3600);
    m = Math.floor(ss / 60);
    ss -= (m * 60);
    s = Math.floor(ss);
    fract = style.delimiter + (""+(ss - s)).substr(2, 3);
    while (fract.length < 4) { fract += "0"; } // PAD TO ALWAYS BE THREE DIGITS LONG (eg .040)
    ret = (h || style.requireHours ? ((((h<10)?"0":"")+h)+":") : '')
    ret += (((m<10)?"0":"")+m)+":"+(((s<10)?"0":"")+s)+fract;
    return ret;
}

timecode_styles = {
    'srt': {
        requireHours: true,
        delimiter: ','
    },
    'html5': {
        requireHours: false,
        delimiter: '.'
    }
}

function aa_timecodeToSeconds (tc) {
    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/,
        groups = tcpat.exec(tc);
    if (groups != null) {
        var h = groups[1] !== undefined ? parseInt(groups[1]) : 0,
            m = parseInt(groups[2]),
            s = parseFloat(groups[3].replace(/,/, "."));
        return (h * 3600) + (m * 60) + s;
    }
}

function parse_fragment (fragment) {
	var parts = fragment.split('&'),
		part, m;
	for (var i=0, l=parts.length; i<l; i++) {
		part = parts[i];
		m = part.match(/^t=(.+?)(?:,(.+))?$/);
		if (m !== null) {
			this.timeStart = m[1];
			if (m[2]) {
				this.timeEend = m[2];
			}
		}
		m = part.match(/^line=(.+?)(?:,(.+))?$/);
		if (m !== null) {
			this.lineStart = parseInt(m[1]);
			if (m[2]) {
				this.lineEnd = parseInt(m[2]);
			}
		}
	}
}

function parse_href (href) {
	var that = {
			href: href
		},
		hashpos = href.indexOf('#'),
		base = href,
		fragment = null;
	if (hashpos >= 0) {
		base = href.substr(0, hashpos);
		fragment = href.substr(hashpos+1);
		parse_fragment.call(that, fragment);
	}
	that['base' ] = base;
	that['nofrag' ] = base;
	that['basehref' ] = base;
	that['fragment'] = fragment;
	var lsp = base.lastIndexOf("/");
	that['basename'] = (lsp !== -1) ? base.substr(lsp+1) : base;
	return that;
}






function editor (elt, ace) {
    ace = ace || window.ace;

    var that = {},
    	aceeditor,
    	sessionsByHref = {},
	    modelist = ace.require("ace/ext/modelist"),
	    TokenClicks = ace.require('kitchen-sink/token_clicks').TokenClicks,
        tokenclicks,
        current_href,
        highlightLineStart,
        highlightLineEnd,
        root = elt,
        body = document.getElementById("editorbody"),
        controls = document.getElementById("controls"),
        ctrigger = document.getElementById("controltrigger"),
        modeselector = document.getElementById("editormode"),
        save = document.getElementById("savebutton"),
        saveas = document.getElementById("saveasbutton"),
        savecopy = document.getElementById("savecopybutton"),
        status = document.getElementById("status"),
        theme_select = document.getElementById("theme"),
        font_select = document.getElementById("font"),
        font_size_select = document.getElementById("fontsize"),
        wrap = document.getElementById("wrap"),
        show_invisible = document.getElementById("showinvisibles");

    that.get_session = function (href) {
        return sessionsByHref[href];
    }

    // Trigger to display controls
    ctrigger.addEventListener("mouseenter", function () {
        controls.style.display = "block";
    });
    controls.addEventListener("mouseleave", function () {
        controls.style.display = "none";
    });

    aceeditor = ace.edit(body);
    $(document).bind("resize", function (e) { aceeditor.resize(); });

    var cur_font = localStorage.getItem("makeserver_editor_font") || "Hack Bold";
    aceeditor.setOptions({fontFamily: cur_font});
    font_select.value = cur_font;
    font_select.addEventListener("change", function () {
        aceeditor.setOptions({fontFamily: this.value });
        localStorage.setItem("makeserver_editor_font", this.value);
    });

    var cur_font_size = localStorage.getItem("makeserver_editor_font_size") || "12px";
    aceeditor.setFontSize(parseInt(cur_font_size));
    font_size_select.value = cur_font_size;
    font_size_select.addEventListener("change", function () {
        aceeditor.setFontSize(parseInt(this.value));
        localStorage.setItem("makeserver_editor_font_size", this.value);
    });

    var cur_show_invisibles = (localStorage.getItem("makeserver_editor_show_invisibles") == "true");
    show_invisible.checked = cur_show_invisibles;
    aceeditor.setShowInvisibles(cur_show_invisibles);
    show_invisible.addEventListener("change", function () {
        aceeditor.setShowInvisibles(this.checked);
        localStorage.setItem("makeserver_editor_show_invisibles", this.checked ? "true" : "");
    });

    var cur_wrap = (localStorage.getItem("makeserver_editor_wrap") == "true");
    wrap.checked = cur_wrap;
    aceeditor.setOption("wrap", cur_wrap);
    wrap.addEventListener("change", function () {
        cur_wrap = this.checked;
        aceeditor.setOption("wrap", this.checked);
        localStorage.setItem("makeserver_editor_wrap", this.checked ? "true" : "");
    });

    var cur_gutter = (localStorage.getItem("makeserver_editor_gutter") == "true");
    gutter.checked = cur_gutter;
    aceeditor.renderer.setShowGutter(cur_gutter);
    gutter.addEventListener("change", function () {
        aceeditor.renderer.setShowGutter(this.checked);
        localStorage.setItem("makeserver_editor_gutter", this.checked ? "true" : "");
    });

    activeline.checked = false;
    if (activeline.checked) {
        aceeditor.setHighlightActiveLine(true);
    }
    activeline.addEventListener("change", function () {
        aceeditor.setHighlightActiveLine(this.checked);
    })

    // Mode
    modeselector.addEventListener("change", function () {
        aceeditor.getSession().setMode(this.value);
    }, false);
    for (var i=0, l=modelist.modes.length; i<l; i++) {
        var opt = document.createElement("option"),
            d = modelist.modes[i];
        opt.value = d.mode;
        opt.textContent = d.caption;
        modeselector.appendChild(opt);
    }

    // Theme (sticky)
    var cur_theme = localStorage.getItem("makeserver_editor_theme", this.value);
    if (cur_theme) {
        aceeditor.setTheme(cur_theme)
        theme.value = cur_theme;
    }
    theme.addEventListener("change", function () {
        aceeditor.setTheme(this.value)
        localStorage.setItem("makeserver_editor_theme", this.value);
    });

    aceeditor.renderer.setShowPrintMargin(false);

    // debugging
    window.aceeditor = aceeditor;

    tokenclicks = new TokenClicks(aceeditor, {
        tokenChange: function (token) {
            // console.log("token", token.type)
            if (token != null && (
                (token.type == "heading.timecode.start") ||
                (token.type == "heading.timecode.end") )) {
                return true;
            }
            if (token != null && 
                token.type == "markup.underline") {
                return true;
            }
        }
    });
    tokenclicks.on("tokenclick", function (token) {
        // console.log("tokenclick", token);
        if ((token.type == "heading.timecode.start") || (token.type == "heading.timecode.end")) {
            var t = aa_timecodeToSeconds(token.value),
                tc;
            if (t) {
                // console.log("timecode click", t);
                window.postMessage({
                    msg: "fragmentclick",
                    origin: window.name,
                    startNormalized: t,
                    start: aa_secondsToTimecode(t)
                }, "*");
                var tc = aa_secondsToTimecode(t);
                // console.log("aafragment", tc);
                localStorage.setItem("cccontrol", JSON.stringify({
                    cmd: "fragmentclick",
                    value: token.value,
                    time: new Date()
                }));
                // var href = current_href.replace(/\.srt$/, '');
                // $(editor.elt).trigger("fragmentclick", { href: href+"#t="+aa.secondsToTimecode(t) });
            } else {
                console.log("bad timecode");
            }
        } else if (token.type == "markup.underline") {
            var clickhref = parse_href(token.value);
            localStorage.setItem("cccontrol", JSON.stringify({
                cmd: "fragmentclick",
                value: token.value,
                time: new Date()
            }))
        }
    });

    function do_save () {
        var text = aceeditor.getValue();
        status.innerHTML = "saving...";
        post(current_href, {text: text}, function (err, resp) {
            if (err) {
                console.log("post ERROR", err);
            } else {
                console.log("post", resp);
                status.innerHTML = "Saved " + new Date();
            }
        })
    }
    function do_save_as (msg, reset_current) {
        var newname = prompt(msg),
            text = aceeditor.getValue(),
            new_href = current_href.replace(/\/([^\/]+)$/, "/"+newname);
        // console.log("NEW_HREF", new_href);
        status.innerHTML = "saving...";
        post(new_href, {text: text}, function (err, resp) {
            if (err) {
                console.log("post ERROR", err);
            } else {
                console.log("post", resp);
                status.innerHTML = "saved " + new Date();
                if (reset_current) {
                    that.href(new_href);
                }
            }
        })
    }
    save.addEventListener("click", do_save, false);
    saveas.addEventListener("click", function () {
            do_save_as("save as...", true);
    }, false);
    savecopy.addEventListener("click", function () {
            do_save_as("save a copy...", false);
    });


    function highlight(s, e) {
        var session = aceeditor.getSession();
        if (highlightLineStart) {
            for (var i=(highlightLineStart-1); i<=(highlightLineEnd-1); i++) {
                session.removeGutterDecoration(i, 'ace_gutter_active_annotation');
            }
        }
        highlightLineStart = s;
        highlightLineEnd = e;
        if (highlightLineStart) {
            for (var i=(highlightLineStart-1); i<=(highlightLineEnd-1); i++) {
                session.addGutterDecoration(i, 'ace_gutter_active_annotation');
            }
            aceeditor.scrollToLine(highlightLineStart, true, true);
        }
    }

    that.href = function (href, done) {
        if (arguments.length == 0) {
            var ret = current_href;
            if (highlightLineStart) {
                ret = lineRangeHref(current_href, highlightLineStart, highlightLineEnd);
            }
            return ret;
        }
        // console.log("href", href, done);
        href = parse_href(href);
        // console.log("parsed", href);
        var session = sessionsByHref[href.nofrag];
        current_href = href.nofrag;

        if (session == "loading") {
            return false;
        }

        if (session != undefined) {
            if (done) {
                window.setTimeout(function () {
                    done.call(session);
                }, 0);
            }
            aceeditor.setSession(session.acesession);
            // deal with eventual changed fragment
            if (href.lineStart) {
                highlight(href.lineStart, href.lineEnd);
                $(editor.elt).trigger("fragmentupdate", {editor: editor});
            }

            return true;
        }

        sessionsByHref[href.nofrag] = "loading";
        $.ajax({
            url: href.nofrag,
            data: { f: (new Date()).getTime() },
            dataType: 'text',
            success: function (data) {
                // console.log("got data", data);
                var mode = modelist.getModeForPath(href.nofrag).mode || "ace/mode/text",
                    session = { href: href.nofrag, editor: editor };
                
                session.acesession = ace.createEditSession(data, mode);
                // index(href.nofrag, data);
                modeselector.value = mode;
                aceeditor.setSession(session.acesession);
                // editor.setOption("showLineNumbers", false);
                aceeditor.setHighlightActiveLine(false);
                session.acesession.setUseWrapMode(cur_wrap);
                sessionsByHref[href.nofrag] = session;
                if (done) {
                    window.setTimeout(function () {
                        done.call(session);
                    }, 0);
                }
            },
            error: function (code) {
                console.log("aceeditor: error loading", href.nofrag, code);
            }
        });
    }

    // ??? 
    var observed_href = null;
    $(document).on("fragmentupdate", function (e, data) {
        if ((e.target !== editor.elt) && data.editor) {
            observed_href = data.editor.href();
            // console.log("ace.fragmentupdate", e.target, observed_href);
        }
    });

    function bind_keys (e) {
        e.commands.addCommand({
            name: 'pasteTimecode',
            bindKey: {win: 'ctrl-shift-down',  mac: 'command-shift-down'},
            exec: function () {
                var mode = aceeditor.getSession().getMode().$id,
                    link,
                    t;
                console.log("paste using localStorage", window, window.parent);
                var cchref = localStorage.getItem("cchref"),
                    ccfragment = localStorage.getItem("ccfragment");

                if (cchref) {
                    if (mode == "ace/mode/srtmd") {
                        link = ccfragment+" -->\n";
                    } else if (mode == "ace/mode/markdown") {
                        if (ccfragment) {
                            link = "[#"+ccfragment+"]("+cchref+"#"+ccfragment+")";
                        } else {
                            link = "["+cchref+"]("+cchref+")";
                        }
                    } else {
                        link = cchref+"#"+ccfragment;
                    }
                    aceeditor.insert(link);
                }
            },
            readOnly: false
        });
        e.commands.addCommand({
            name: 'save',
            bindKey: {win: 'ctrl-s',  mac: 'command-s'},
            exec: function () {
                do_save();
            },
            readOnly: false
        });
        e.commands.addCommand({
            name: 'toggleMedia',
            bindKey: {win: 'ctrl-shift-up',  mac: 'command-shift-up'},
            exec: function () {
                window.postMessage({msg: "cc_toggle", origin: window.name}, "*");
                localStorage.setItem("cccontrol", JSON.stringify({cmd: "toggle", time: new Date()}));
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        e.commands.addCommand({
            name: 'jumpMediaBack',
            bindKey: {win: 'ctrl-shift-left',  mac: 'command-shift-left'},
            exec: function () {
                window.postMessage({msg: "cc_left", origin: window.name}, "*");
                localStorage.setItem("cccontrol", JSON.stringify({cmd: "left", time: new Date()}));
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        e.commands.addCommand({
            name: 'jumpMediaForward',
            bindKey: {win: 'ctrl-shift-right',  mac: 'command-shift-right'},
            exec: function () {
                window.postMessage({msg: "cc_right", origin: window.name}, "*");
                localStorage.setItem("cccontrol", JSON.stringify({cmd: "right", time: new Date()}));
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        /*
        e.commands.addCommand({
            name: 'newSession',
            bindKey: {win: 'ctrl-n',  mac: 'command-n'},
            exec: function () {
                // console.log("NEW");
                editor.newSession();
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        */
    }
    bind_keys(aceeditor);

    return that;
}

window.editor = editor;

})();
