#!/usr/bin/env python

import mimetypes, datetime, os, json
import cgi
import cgitb; cgitb.enable()
import urllib, urlparse
from uri_to_path import *


def stat(fp):
    data = {}
    # data['exists'] = os.path.exists(fp)
    data['is_file'] = os.path.isfile(fp)
    _, filename = os.path.split(fp.rstrip('/'))
    data['filename'] = filename
    is_dir = os.path.isdir(fp)
    data['is_dir'] = is_dir
    if is_dir:
        data['type'] = "directory"
    else:
        data['type'] = "file"
    stat = os.stat(fp)
    if is_dir:
        data['mime'] = "inode/directory"
    else:
        mime, _ = mimetypes.guess_type(fp, strict=False)
        if mime:
            data['mime'] = mime
        data['bytes'] = stat.st_size
    # data['mtime'] = stat.st_mtime
    data['mtime_iso'] = datetime.datetime.fromtimestamp(stat.st_mtime).isoformat()
    return data

def ls (path):
    out = stat(path)
    out['id'] = path_to_uri(path, os.environ)

    cc = os.listdir(path)
    cc.sort()
    out['files'] = files = []
    for x in cc:
        fp = os.path.join(path, x)
        d = stat(fp)
        d['id'] = path_to_uri(fp, os.environ)
        if d['is_dir']:
            ruri = path_to_uri(fp, os.environ, relative=True)
            d['continue'] =  "/cookbook/cgi-bin/ls.cgi?" + urllib.urlencode({"uri": ruri}) 

        # ruri = path_to_uri(fp, os.environ, relative=True)
        files.append(d)

    return out

# ref = env.get("HTTP_REFERER")
fs = cgi.FieldStorage()
ruri = fs.getvalue("uri", "")
path = uri_to_path(ruri, os.environ)
if path and os.path.isdir(path):
    out = ls(path)

print "Content-type: application/json"
print
print json.dumps(out)

