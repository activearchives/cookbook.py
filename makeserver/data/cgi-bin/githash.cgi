#!/usr/bin/env python
import hashlib, os, json
import cgi
import cgitb; cgitb.enable()
from uri_to_path import *


def githash (p):
    # http://stackoverflow.com/questions/552659/assigning-git-sha1s-without-git
    data = {}
    # data['path'] = p
    h = hashlib.sha1()
    stat = os.stat(p)
    h.update("blob {0}\0".format(stat.st_size).encode("utf-8")) 
    with open (p, "rb") as f:
        while True:
            fdata = f.read()
            if not fdata:
                break
            h.update(fdata)
    data['hash'] = sha = h.hexdigest()
    return data


fs = cgi.FieldStorage()
ruri = fs.getvalue("uri", "")
path = uri_to_path(ruri, os.environ)

if os.path.isfile(path):
    out = githash(path)
    out['id'] = path_to_uri(path, os.environ)
    print "Content-type: application/json"
    print
    print json.dumps(out)
else:
    print "Content-type:text/plain"
    print
    print path

