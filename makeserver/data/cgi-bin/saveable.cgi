#!/usr/bin/env python

import cgi, json, sys, os
from urlparse import urlparse

env = os.environ
meth = env.get("REQUEST_METHOD", "GET").upper()

allow_path = True

if meth == "POST":
    print "Content-type: application/json"
    print
    fs = cgi.FieldStorage()
    pwd = env.get("PWD", env.get("PATH_TRANSLATED"))
    path = fs.getvalue("path", "").lstrip("/")
    referer = ""
    if (not allow_path) or (not path):
        referer = env.get("HTTP_REFERER")
        referer = urlparse(referer)
        path = referer.path.lstrip("/")
        if path == "":
            path = "index.html"
        path = os.path.join(pwd, path)
    text = fs.getvalue("text", "")

        # backup
    try:
        if os.path.exists(path):
            if os.path.exists(path+"~"): # for windows (rename fails if ~ exists)
                os.remove(path+"~")
            os.rename(path, path+"~")

        with open(path, "w") as f:
            f.write(text)
        print json.dumps({
            "success": True,
            "path": path,
            "pwd": pwd,
            "referer": referer,
            "msg": "saved {0} bytes".format(len(text))
        })
    except OSError as e:
        print json.dumps({
            "success": False,
            "path": path,
            "pwd": pwd,
            "referer": referer,
            "error": str(e)
        })

else:
    this_url = os.environ.get("SCRIPT_NAME", "")
    print "Content-type: application/javascript"
    print
    print """
(function () {
    // http://blog.garstasio.com/you-dont-need-jquery/ajax/
    function $post(url, data, success) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            
        xhr.onload = function() {
            console.log(xhr.status);
            if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                success.call(xhr, response)
            }
        };
        //xhr.send(JSON.stringify(data));
        xhr.send("text="+encodeURIComponent(data));
    }

    function save () {
        // http://stackoverflow.com/questions/6088972/get-doctype-of-an-html-as-string-with-javascript
        var node = document.doctype,
            html = "";
        if (node) {
            html = "<!DOCTYPE "
                 + node.name
                 + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '')
                 + (!node.publicId && node.systemId ? ' SYSTEM' : '') 
                 + (node.systemId ? ' "' + node.systemId + '"' : '')
                 + ">\\n";
        }
        html += document.documentElement.outerHTML;
        $post(SAVEABLE_URL, html, function (data) {
            console.log("saved", data);
        }); 
    }

    document.addEventListener("keydown", function (e) {
        // console.log("keydown", e);
        if (e.key == "s" && e.ctrlKey) {
            e.preventDefault();
            save();
        }
    });

    window.saveable_save = save;
})();
""".replace("SAVEABLE_URL", '"'+this_url+'"');

