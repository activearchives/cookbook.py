#!/usr/bin/env python
import hashlib, os, json
import cgi, sys
import cgitb; cgitb.enable()
from uri_to_path import *
import subprocess

env = os.environ
pwd = env.get("PWD", env.get("PATH_TRANSLATED"))
fs = cgi.FieldStorage()
path = fs.getvalue("path", "").strip("/")

if path:
    abspath = os.path.join(pwd, path)
    try:
        result = subprocess.call(["touch", abspath])
        print "Content-type: application/json;charset=utf-8"
        print
        print json.dumps({'result': result})
        sys.exit(0)
    except IOError as e:
        print json.dumps({'result': result, msg: e.toString()})
        sys.exit(0)

print "Content-type: text/html"
print
print "nothing to do"
