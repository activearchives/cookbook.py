#!/usr/bin/env python

import os
import cgi
import urlparse
import urllib
import subprocess
import re
from xml.sax.saxutils import quoteattr

import cgitb; cgitb.enable()

MAKE = "make"
MAKEFILE = None

fs = cgi.FieldStorage()
pwd = fs.getvalue("pwd")
uri = fs.getvalue("uri")

def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX
    # Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    # eg /home/murtaugh/Music ... path to current docroot

    # Strip CONTEXT_PREFIX from uri
    # eg /music... the URI component corresponding to the CDR
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

if not uri:
    print "Content-type:text/plain"
    print
    print "no uri"
    import sys
    sys.exit(0)

filepath = uri_to_path(uri, os.environ)

args = [MAKE]
if MAKEFILE:
    args.append("-f")
    args.append(MAKEFILE)
args.append("-n")
args.append("-B")
args.append(filepath)
# try:
output = subprocess.check_output(args, cwd=pwd)
# except subprocess.CalledProcessError, e:
#     output = ""
print "Content-type:text/html;charset=utf-8"
print
print """<!DOCTYPE>
<html>
<head>
<script src="/cookbook/ace-builds/src-noconflict/ace.js"></script>
<style>
textarea.code {{
    width: 100%;
    height: 480px;
    font-family: monospace;
    display: block;
    resize: vertical;
}}
.ace_editor {{
    width: 100%;
    height: 480px;
    position: relative;
    }}
#textarea_send {{
    display: none;
}}
</style>
</head>
<body>
<h1>Recipe to make {0[file]}</h1>
<textarea id="editor" class="code">{0[script]}</textarea>
<form id="form" method="post" action="{0[formurl]}">
<textarea name="script" id="textarea_send"></textarea>
<input type="submit" value="run" />
<input type="hidden" name="pwd" value={0[pwdattr]} />
</form>""".format({
    'file': file,
    'formurl': '/cookbook/cgi-bin/shell.cgi?' + urllib.urlencode({}),
    'script': output,
    'pwdattr': quoteattr(pwd)
})
print """
<script>
    var textarea_edit = document.getElementById("editor"),
        textarea_send = document.getElementById("textarea_send"),
        form = document.getElementById("form"),
        editor = ace.edit(textarea_edit);
    editor.getSession().setMode("ace/mode/sh");
    form.addEventListener("submit", function (e) {
        // e.preventDefault();
        var code = editor.getValue();
        console.log("code", code);
        textarea_send.value = code;
        return true;
    })
</script>
"""

print """
</body>
</html>
"""
