#!/usr/bin/env python

import os
import cgi
import urlparse
import urllib
import subprocess
import re
from xml.sax.saxutils import quoteattr

import cgitb; cgitb.enable()

MAKE = "make"
MAKEFILE = None

fs = cgi.FieldStorage()
pwd = fs.getvalue("pwd")
uri = fs.getvalue("uri")

def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX
    # Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    # eg /home/murtaugh/Music ... path to current docroot

    # Strip CONTEXT_PREFIX from uri
    # eg /music... the URI component corresponding to the CDR
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

if not uri:
    print "Content-type:text/plain"
    print
    print "no uri"
    import sys
    sys.exit(0)

filepath = uri_to_path(uri, os.environ)

args = [MAKE]
if MAKEFILE:
    args.append("-f")
    args.append(MAKEFILE)

if fs.getvalue("force", False):
    args.append("-B")

args.append(filepath)

try:
    output = subprocess.check_output(args, cwd=pwd, stderr=subprocess.STDOUT)
    referer = os.environ.get("HTTP_REFERER")
    if referer and fs.getvalue("return"):
        print "Location: {0}".format(referer)
        print
    else:
        print "Content-type:text/plain;charset=utf-8"
        print
        print "Make sucessful"
        print output

except subprocess.CalledProcessError, e:
    print "Content-type:text/plain;charset=utf-8"
    print 
    print "ERROR", e.returncode
    print e.output

