import datetime, stat, hashlib, os

def git_hash (fp):
    sha1 = hashlib.sha1()
    size = os.path.getsize(fp)
    sha1.update("blob{0}\0".format(size))
    with open(fp) as f:
        while True:
            bytes = f.read()
            if not bytes:
                break
            sha1.update(bytes)
    return sha1.hexdigest()

def humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """
    abbrevs = (
        (1<<50L, 'PB'),
        (1<<40L, 'TB'),
        (1<<30L, 'GB'),
        (1<<20L, 'MB'),
        (1<<10L, 'kB'),
        (1, 'bytes')
    )
    if bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    ret = '%.*f' % (precision, bytes / factor)
    if ret.endswith(".0"):
        ret = ret[:-2]
    ret += " "+suffix
    return ret

def fmt_mode (m):
    ret = ''
    # if stat.S_ISDIR(m):
    #     ret = "d"
    # else:
    #     ret = " "
    # ret += " "

    def _rwx(m, flags):
        ret = ''
        for flag, c in zip(flags, "rwx"):
            if m & flag:
                ret += c
            else:
                ret += "&ndash;"
        return ret

    ret += _rwx(m, (stat.S_IRUSR, stat.S_IWUSR, stat.S_IXUSR))
    ret += "|"
    ret += _rwx(m, (stat.S_IRGRP, stat.S_IWGRP, stat.S_IXGRP))
    ret += "|"
    ret += _rwx(m, (stat.S_IROTH, stat.S_IWOTH, stat.S_IXOTH))

    return ret

