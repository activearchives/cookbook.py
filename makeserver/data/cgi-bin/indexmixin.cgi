#!/usr/bin/env python

from __future__ import division
import os, sys, json, cgi, re, urlparse, urllib, subprocess, datetime
import cgitb; cgitb.enable()


# print "Content-type:text/html"
# print
# cgi.print_environ()
# import sys
# sys.exit(0)
def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX, Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

def path_to_uri (path, env, relative=False):
    is_dir = os.path.isdir(path)
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    cdr = cdr.rstrip("/")
    if path.startswith(cdr):
        path = path[len(cdr):]
        ret = ""
        if not relative:
            ret = "http://" + env.get("SERVER_NAME")
            p = int(env.get("SERVER_PORT", "80"))
            if p != 80:
                ret += ":" + str(p)
        ret += urllib.quote(path)
        if is_dir and not ret.endswith("/"):
            ret = ret + "/"
        return ret

#############
# PART I: Script

fs = cgi.FieldStorage()
format = fs.getvalue("format")
script_name = os.environ.get("SCRIPT_NAME")

if not format:
    print "Content-type: application/javascript"
    print
    print """

// refresh the page data
document.addEventListener("DOMContentLoaded", function () {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'THIS?format=json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            var data = JSON.parse(xhr.responseText);
            // console.log("merging data with page", data);
            pagedata = window.get_situated_data()
            pagedata.join(data, "MediaObject");
            // ... to make a "live page" ... add this to an interval (or have an in page "data reload" button)
        }
        else {
            console.log('xhr Request failed.  Returned status of ' + xhr.status);
        }
    };
    xhr.send();
        
});

""".replace("THIS", script_name)
    sys.exit(0)


#############
# PART II: CGI


# def baseurl (u):
#     p = urlparse.urlparse(u)
#     return urlparse.urlunparse((p.scheme, p.netloc, p.path, None, None,None))

# Get the Relative URL of the referring page
# Then translate this to a local path

ruri = urlparse.urlparse(os.environ.get("HTTP_REFERER"))
ruri = ruri.path.lstrip('/')
label = urllib.unquote(ruri)
path = uri_to_path(ruri, os.environ)

# parent_link = '/'.join( request_uri.strip('/').split('/')[:-1] )
# if parent_link:
#     parent_link = "/" + parent_link
# elif request_uri != '/':
#     parent_link = '/'

# print "Content-type: application/json"
# print
# print json.dumps(dict(os.environ), indent=2)
# sys.exit(0)


def strftime (x, template="%Y-%m-%d %H:%M:%S"):
    return x.strftime(template)

def is_filename (f):
    base, ext = os.path.splitext(f)
    return len(ext) > 1 or f.endswith("/")

def humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)                   '1 byte'
    >>> humanize_bytes(1024)                '1.0 kB'
    >>> humanize_bytes(1024*123)            '123.0 kB'
    >>> humanize_bytes(1024*12342)          '12.1 MB'
    >>> humanize_bytes(1024*12342,2)        '12.05 MB'
    >>> humanize_bytes(1024*1234,2)         '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)    '1.3 GB'
    """
    abbrevs = (
        (1<<50L, 'PB'),
        (1<<40L, 'TB'),
        (1<<30L, 'GB'),
        (1<<20L, 'MB'),
        (1<<10L, 'kB'),
        (1, 'bytes')
    )
    if bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    return '%.*f %s' % (precision, bytes / factor, suffix)


def is_binary_file (p):
    """ returns none on ioerror """
    # http://stackoverflow.com/questions/898669/how-can-i-detect-if-a-file-is-binary-non-text-in-python
    textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
    is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
    try:
        return not os.path.isdir(p) and is_binary_string(open(p, 'rb').read(1024))
    except IOError:
        return None

def is_text_file (p):
    """ returns none on ioerror """
    textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
    is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
    if os.path.isfile(p):
        try:
            return not is_binary_string(open(p, 'rb').read(1024))
        except IOError:
            return None

def make_n (path, makefile=None, make="make"):
    args = [make]
    if makefile:
        args.append("-f")
        args.append(makefile)
    args.append("-n")
    args.append("--debug=v")

    try:
        output = subprocess.check_output(args, cwd=path)
    except subprocess.CalledProcessError, e:
        output = ""

    # debug output
    # print "<pre>"+output+"</pre>"

    makeable = set()
    prereqpat = re.compile(r"^\s*Prerequisite '(.+?)' is older than target '(.+?)'.\s*$", re.M)
    for a, b in prereqpat.findall(output):
        if is_filename(b):
            makeable.add(b)

    remakepat = re.compile(r"^\s*Must\ remake\ target\ [\`'](.+?)\'\.\s*$", re.M)
    remake = set()
    for x in remakepat.findall(output):
        if is_filename(x):
            remake.add(x)

    files = os.listdir(path)
    def name(x):
        fp = os.path.join(path, x)
        if os.path.isdir(fp):
            return x+"/"
        return x
    files = [name(x) for x in files]
    missingpat = re.compile(r"^\s*File\ [\`'](.+?)\'\ does\ not\ exist\.\s*$", re.M)
    missing = set()
    for x in missingpat.findall(output):
        if is_filename(x):
            missing.add(x)
            files.append(x)
    files.sort()

    def wrap (x):
        fp = os.path.join(path, x)
        is_text = is_text_file(fp)
        return (x, os.path.isdir(fp), x in makeable, x in remake, x in missing, is_text)

    return [wrap(x) for x in files]


# def abspath (env):
#     root = env.get("DOCUMENT_ROOT")
#     p = urlparse.urlparse(env.get("REQUEST_URI"))
#     if p.path.startswith("/"):
#         path = p.path.lstrip("/")
#         path = urllib.unquote(path)
#         return os.path.join(root, path)

    # os.path.join(path, env.get("REQUEST_URI"))

# locate the makefile to be used
# evt. put restrictions on this!
docroot = os.environ.get("PWD")

use_makefile = None
makefile = os.path.join(docroot, "makefile")
if os.path.exists(makefile):
    use_makefile = makefile
if path != docroot:
    makefile = os.path.join(path, "makefile")
    if os.path.exists(makefile):
        use_makefile = makefile

# print "Content-type: application/json"
# print
# print json.dumps({'mpath': mpath}, indent=2)
# sys.exit(0)

files = make_n(path, use_makefile)

# tvars = {
#     'request_label': request_label,
#     'parent_link': parent_link
# }


items = []
for file, is_dir, makeable, remake, missing, is_text in files:
    link = urllib.quote(file)
    filename = file
    fp = os.path.join(path, file)
    try:
        stat = os.stat(fp)
        lastmod = datetime.datetime.fromtimestamp(stat.st_mtime)
        lastmod = lastmod.isoformat()
        # lastmod = lastmod.strftime("%Y-%m-%d %H:%M:%S")
        fsize = stat.st_size # humanize_bytes(stat.st_size, 0)
    except OSError:
        lastmod = None # "&mdash;"
        fsize = None
    file_request_uri = ruri + urllib.quote(file)
    items.append({
        'id': link,
        'url': link,
        'is_dir': is_dir,
        'filename': filename.decode("utf-8"),
        'dateModified': lastmod,
        'contentSize': fsize,
        'makeable': makeable,
        'remake': remake,
        'missing': missing,
        'is_text': is_text
    })

# tvars['items'] = items
# print index.render(tvars).encode("utf-8")

print "Content-type: application/json"
print

items.sort(key=lambda x: x['filename'].lower())

print json.dumps(items, indent=2)

# <style type="text/css">
#     div.file {
#     }
#     div.file a.url {
#         display: inline-block;
#         min-width: 200px;
#     }
#     div.file .dateModified {
#         display: inline-block;
#         min-width: 200px;
#     }
#     div.file .contentSize {
#         display: inline-block;
#         min-width: 100px;
#     }
# </style>
