#!/usr/bin/env python

import os
import cgi
import urlparse
import urllib
import subprocess
import re
from xml.sax.saxutils import quoteattr

import cgitb; cgitb.enable()
import pipes

fs = cgi.FieldStorage()
pwd = fs.getvalue("pwd")
script = fs.getvalue("script")
if not script:
    print "Content-type:text/plain"
    print
    print "no script"
    import sys
    sys.exit(0)

# try:
# cmd = pipes.quote(script)
# cmd = re.sub(r"\t+", " ", cmd)
try:
    # fix them pesky windows style newlines (that posting seems to introduce)
    script = re.sub(r"\r\n", "\n", script)
    # cmd = re.sub(r"\\[\r\n]+", " ", cmd)
    # output = subprocess.check_output(script, shell=True, cwd=pwd, stderr=subprocess.STDOUT)
    p = subprocess.Popen(["bash"], stdin=subprocess.PIPE, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, cwd=pwd)
    # script = open("test.sh").read()
    output, _ = p.communicate(script)
except subprocess.CalledProcessError, e:
     print "Content-type:text/plain;charset=utf-8"
     print
     # print dir(e)
     print "ERROR: returncode {0}".format(e.returncode)
     print
     print e.output
     import sys
     sys.exit(0)

referer = os.environ.get("HTTP_REFERER")

print "Content-type:text/html;charset=utf-8"
print
print """<!DOCTYPE>
<html>
<head>
<style>
textarea.code {{
    width: 100%;
    height: 180px;
    font-family: monospace;
    display: block;
    resize: vertical;
}}
textarea.output {{
    width: 100%;
    height: 480px;
    font-family: monospace;
    display: block;
    resize: vertical;
}}
</style>
</head>
<body>
<h2>Script</h2>
<textarea class="code">{0[script]}</textarea>
<h2>output</h2>
<textarea class="code" name="script">{0[output]}</textarea>
<a href="{0[referer]}">back</a>
</form>
</body>
</html>
""".format({
    'referer': referer,
    'file': file,
    'formurl': '' + urllib.urlencode({}),
    'script': script,
    'output': output,
    'pwdattr': quoteattr(pwd)
})

