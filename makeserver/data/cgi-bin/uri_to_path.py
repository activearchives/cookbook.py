import urllib, os

def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX
    # Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    # eg /home/murtaugh/Music ... path to current docroot
    # Strip CONTEXT_PREFIX from uri
    # eg /music... the URI component corresponding to the CDR
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

def path_to_uri (path, env, relative=False):
    is_dir = os.path.isdir(path)
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    cdr = cdr.rstrip("/")
    if path.startswith(cdr):
        path = path[len(cdr):]
        ret = ""
        if not relative:
            ret = "http://" + env.get("SERVER_NAME")
            p = int(env.get("SERVER_PORT", "80"))
            if p != 80:
                ret += ":" + str(p)
        ret += urllib.quote(path)
        if is_dir and not ret.endswith("/"):
            ret = ret + "/"
        return ret
