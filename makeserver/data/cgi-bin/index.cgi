#!/usr/bin/env python

from __future__ import division
import os
import cgi
import urlparse
import urllib
import subprocess
import re
import datetime
import jinja2
from jinja2 import Environment, FileSystemLoader

import cgitb; cgitb.enable()

MAKE = "make"
MAKEFILE = None


# print "Content-type:text/html"
# print
# cgi.print_environ()
# import sys
# sys.exit(0)

templates = FileSystemLoader("./templates")
env = Environment(loader=templates)

def strftime (x, template="%Y-%m-%d %H:%M:%S"):
    return x.strftime(template)

env.filters['strftime'] = strftime

# print "Content-type: text/plain"
# print
# print __file__
index = env.get_template("index.html")

def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX
    # Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    # eg /home/murtaugh/Music ... path to current docroot

    # Strip CONTEXT_PREFIX from uri
    # eg /music... the URI component corresponding to the CDR
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

request_uri = os.environ.get("REQUEST_URI")
# print uri_to_path(request_uri, os.environ)
# import sys
# cgi.print_environ()
# sys.exit(0)


def is_filename (f):
    base, ext = os.path.splitext(f)
    return len(ext) > 1 or f.endswith("/")

def humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """
    abbrevs = (
        (1<<50L, 'PB'),
        (1<<40L, 'TB'),
        (1<<30L, 'GB'),
        (1<<20L, 'MB'),
        (1<<10L, 'kB'),
        (1, 'bytes')
    )
    if bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    return '%.*f %s' % (precision, bytes / factor, suffix)

# http://stackoverflow.com/questions/898669/how-can-i-detect-if-a-file-is-binary-non-text-in-python
textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
def is_binary_file (p):
    """ returns none on ioerror """
    try:
        return not os.path.isdir(p) and is_binary_string(open(p, 'rb').read(1024))
    except IOError:
        return None

def is_text_file (p):
    """ returns none on ioerror """
    if os.path.isfile(p):
        try:
            return not is_binary_string(open(p, 'rb').read(1024))
        except IOError:
            return None

def make_n (path):
    args = [MAKE]
    if MAKEFILE:
        args.append("-f")
        args.append(MAKEFILE)
    args.append("-n")
    args.append("--debug=v")

    try:
        output = subprocess.check_output(args, cwd=path)
    except subprocess.CalledProcessError, e:
        output = ""

    # debug output
    # print "<pre>"+output+"</pre>"

    makeable = set()
    prereqpat = re.compile(r"^\s*Prerequisite '(.+?)' is older than target '(.+?)'.\s*$", re.M)
    for a, b in prereqpat.findall(output):
        if is_filename(b):
            makeable.add(b)

    remakepat = re.compile(r"^\s*Must\ remake\ target\ [\`'](.+?)\'\.\s*$", re.M)
    remake = set()
    for x in remakepat.findall(output):
        if is_filename(x):
            remake.add(x)

    files = os.listdir(path)
    def name(x):
        fp = os.path.join(path, x)
        if os.path.isdir(fp):
            return x+"/"
        return x
    files = [name(x) for x in files]
    missingpat = re.compile(r"^\s*File\ [\`'](.+?)\'\ does\ not\ exist\.\s*$", re.M)
    missing = set()
    for x in missingpat.findall(output):
        if is_filename(x):
            missing.add(x)
            files.append(x)
    files.sort()

    def wrap (x):
        fp = os.path.join(path, x)
        is_text = is_text_file(fp)
        return (x, os.path.isdir(fp), x in makeable, x in remake, x in missing, is_text)

    return [wrap(x) for x in files]


# def abspath (env):
#     root = env.get("DOCUMENT_ROOT")
#     p = urlparse.urlparse(env.get("REQUEST_URI"))
#     if p.path.startswith("/"):
#         path = p.path.lstrip("/")
#         path = urllib.unquote(path)
#         return os.path.join(root, path)

    # os.path.join(path, env.get("REQUEST_URI"))
fs = cgi.FieldStorage()
sort_c = fs.getvalue("C", "N")
sort_o = fs.getvalue("O")
request_uri = os.environ.get("REQUEST_URI")
request_label = urllib.unquote(request_uri)

parent_link = '/'.join( request_uri.strip('/').split('/')[:-1] )
if parent_link:
    parent_link = "/" + parent_link
elif request_uri != '/':
    parent_link = '/'
# path = abspath(os.environ)
path = uri_to_path(request_uri, os.environ)

files = make_n(path)

print "Content-type:text/html;charset=utf-8"
print
tvars = {
    'request_label': request_label,
    'parent_link': parent_link
}


items = []
for file, is_dir, makeable, remake, missing, is_text in files:
    classes = []
    if is_dir:
        classes.append("dir")
    if is_text:
        classes.append("text")
    if makeable:
        classes.append("makeable")
    if remake:
        classes.append("remake")
    if missing:
        classes.append("missing")
    classes = " ".join(classes)

    link = urllib.quote(file)
    label = file
    # print """<li class="{0}"><a href="{1}">{2}</a></li>""".format(classes, link, label)
    fp = os.path.join(path, file)
    try:
        stat = os.stat(fp)
        lastmod = datetime.datetime.fromtimestamp(stat.st_mtime)
        # lastmod = lastmod.strftime("%Y-%m-%d %H:%M:%S")
        fsize = humanize_bytes(stat.st_size, 0)
    except OSError:
        lastmod = None # "&mdash;"
        fsize = "&mdash;"
    buttons = ' '
    file_request_uri = request_uri + urllib.quote(file)
    # if makeable or missing or remake:
        # note that request_uri DOES get "double encoded" but this seems the most sensible...
        # as it will be received then as a URI, rather than already decoded
        # makelink = "/cookbook/cgi-bin/make.cgi?" + urllib.urlencode({"pwd": path, "uri": file_request_uri, "return": "1", "force": "1"})
        # buttons +=  '<a href="{0}" class="makebutton make">MAKE</a>'.format(makelink)
        # editmakelink = "/cookbook/cgi-bin/maketext.cgi?" + urllib.urlencode({"pwd": path, "uri": file_request_uri})
        # buttons +=  ' <a href="{0}" class="makebutton editmake">EDIT RECIPE</a>'.format(editmakelink)
    if is_text:
        # editlink = "/ace/#href=" + file_request_uri + "&path=" + urllib.quote(fp)
        editlink = "/cookbook/editor.html#href=" + file_request_uri + "&path=" + urllib.quote(fp)
        buttons += ' <a href="{0}" class="makebutton edit">EDIT</a>'.format(editlink)
        touchlink = "/cookbook/cgi-bin/touch.cgi?" + urllib.urlencode({"pwd": path, "uri": file_request_uri, "return": "1"})
        buttons += ' <a href="{0}" class="makebutton edit">TOUCH</a>'.format(touchlink)

    items.append({
        'is_dir': is_dir,
        'link': link,
        'label': label.decode("utf-8"),
        'lastmod': lastmod,
        'size': fsize,
        'buttons': buttons,
        'classes': classes,
    })

tvars['items'] = items
print index.render(tvars).encode("utf-8")
