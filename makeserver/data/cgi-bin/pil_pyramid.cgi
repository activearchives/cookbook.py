#!/usr/bin/env python

from PIL import Image
import os, sys, json
from uri_to_path import *
import cgi

# usage:
# for i in *.JPG; do image_pyramid.py $i .aa 2>> image_pyramid.log ; done

def build_image_pyramid (img, path, template="L{0[L]}.jpg", limit=64, verbose=False):
    """
    Save an "image pyramid", saving progressively smaller versions of the image as "layers"
    numbered from 1 .. n where scaling of 2**n is applied
    stops when resulting width is <= the limit.

    Returns: annotations
    // or (better), pass an annotation object
    // and procedure adds new bodies

    """
    data = []
    w, h = img.size
    sw, sh = im.size
    layer = 1
    bim = img

    while sw > limit or sh > limit:
        scaler = 2 ** layer
        sw, sh = int(w / scaler), int(h / scaler)
        sim = bim.resize((sw,sh), resample=Image.ANTIALIAS)
        filename = template.format({'L': layer})
        outpath = os.path.join(path, filename)
        sim.save(outpath)
        data.append({
            'type': "image/jpeg",
            'id': path_to_uri(outpath, os.environ)
            'width': sim.size[0],
            'height': sim.size[1]
        })
        layer += 1

    return data

fs = cgi.FieldStorage()
ruri = fs.getvalue("uri", "")
thumbs = fs.getvalue("thumbs", "")
path = uri_to_path(ruri, os.environ)

if os.path.isfile(path):
    img = Image.open(path)
    out = {}
    w, h = img.size
    out['width'] = w
    out['height'] = h
    out['format'] = img.format
    out['mode'] = img.mode
    print "Content-type: application/json"
    print
    print json.dumps(out)
