#!/usr/bin/env python

from __future__ import division
import os, urlparse, urllib, subprocess, re, datetime
import jinja2
from jinja2 import Environment, FileSystemLoader


MAKE = "make"

def strftime (x, template="%Y-%m-%d %H:%M:%S"):
    return x.strftime(template)

def uri_to_path (uri, env):
    # Apache CONTEXT_DOCUMENT_ROOT, DOCUMENT_ROOT, CONTEXT_PREFIX
    # Twisted PWD
    cdr = env.get("CONTEXT_DOCUMENT_ROOT", env.get("DOCUMENT_ROOT", env.get("PWD")))
    # eg /home/murtaugh/Music ... path to current docroot

    # Strip CONTEXT_PREFIX from uri
    # eg /music... the URI component corresponding to the CDR
    cp = env.get("CONTEXT_PREFIX", "")
    if cp and uri.startswith(cp):
        uri = uri[len(cp)+1:]

    path = urllib.unquote(uri.lstrip("/"))
    return os.path.join(cdr, path)

def is_filename (f):
    base, ext = os.path.splitext(f)
    return len(ext) > 1 or f.endswith("/")

def humanize_bytes(bytes, precision=0):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """
    abbrevs = (
        (1<<50L, 'PB'),
        (1<<40L, 'TB'),
        (1<<30L, 'GB'),
        (1<<20L, 'MB'),
        (1<<10L, 'kB'),
        (1, '')
    )
    if bytes == 1:
        return '1'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    return '%.*f %s' % (precision, bytes / factor, suffix)

# http://stackoverflow.com/questions/898669/how-can-i-detect-if-a-file-is-binary-non-text-in-python
textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
def is_binary_file (p):
    """ returns none on ioerror """
    try:
        return not os.path.isdir(p) and is_binary_string(open(p, 'rb').read(1024))
    except IOError:
        return None

def is_text_file (p):
    """ returns none on ioerror """
    if os.path.isfile(p):
        try:
            return not is_binary_string(open(p, 'rb').read(1024))
        except IOError:
            return None

def targets (makefile):
    with open(makefile) as f:
        text = f.read().decode("utf-8")
        return re.findall(r"^(\w+):", text, flags=re.M)

def make_n (path, makefile=None):
    output = ""
    if makefile:
        args = [MAKE]
        args.append("-f")
        args.append(makefile)
        args.append("-n")
        args.append("--debug=v")
        try:
            output = subprocess.check_output(args, cwd=path)
        except subprocess.CalledProcessError, e:
            output = ""

    makeable = set()
    prereqpat = re.compile(r"^\s*Prerequisite '(.+?)' is older than target '(.+?)'.\s*$", re.M)
    for a, b in prereqpat.findall(output):
        if is_filename(b):
            makeable.add(b)

    remakepat = re.compile(r"^\s*Must\ remake\ target\ [\`'](.+?)\'\.\s*$", re.M)
    remake = set()
    for x in remakepat.findall(output):
        if is_filename(x):
            remake.add(x)

    files = os.listdir(path)
    def name(x):
        fp = os.path.join(path, x)
        if os.path.isdir(fp):
            return x+"/"
        return x
    files = [name(x) for x in files]
    missingpat = re.compile(r"^\s*File\ [\`'](.+?)\'\ does\ not\ exist\.\s*$", re.M)
    missing = set()
    for x in missingpat.findall(output):
        if is_filename(x):
            missing.add(x)
            files.append(x)
    files.sort()

    def wrap (x):
        fp = os.path.join(path, x)
        is_text = is_text_file(fp)
        return (x, os.path.isdir(fp), x in makeable, x in remake, x in missing, is_text)

    return [wrap(x) for x in files]

class MakeDirectoryListing (object):
    def __init__ (self, makefile, template, docroot, path):
        self.makefile = makefile
        self.template = template
        self.docroot = docroot
        self.path = path
        self.isLeaf = True

    def render (self, request):
        parent_link = None
        rpath = os.path.relpath(self.path, self.docroot)
        if rpath == ".":
            rpath = "/"
        else:
            parent_link, _ = os.path.split(rpath)
            parent_link = urllib.quote("/"+parent_link)
        files = make_n(self.path, self.makefile)

        request.setHeader(b"content-type", b"text/html; charset=utf-8")
        tvars = {
            'request_label': rpath,
            'parent_link': parent_link
        }
        rurl = rpath
        if not rurl.startswith("/"):
            rurl = "/" + rurl
        tvars['rurl'] = rurl
        items = []
        for file, is_dir, makeable, remake, missing, is_text in files:
            classes = []
            if is_dir:
                classes.append("dir")
            if is_text:
                classes.append("text")
            if makeable:
                classes.append("makeable")
            if remake:
                classes.append("remake")
            if missing:
                classes.append("missing")
            classes = " ".join(classes)

            link = urllib.quote(file)
            label = file
            fp = os.path.join(self.path, file)
            try:
                stat = os.stat(fp)
                lastmod = datetime.datetime.fromtimestamp(stat.st_mtime)
                fsize = stat.st_size
            except OSError:
                lastmod = None # "&mdash;"
                fsize = 0
            buttons = ' '

            _, ext = os.path.splitext(file)
            ext = ext[1:].lower()

            items.append({
                'is_dir': is_dir,
                'is_text': is_text,
                'remake': remake,
                'missing': missing,
                'makeable': makeable,
                'link': link,
                'label': label.decode("utf-8"),
                'lastmod': lastmod,
                'size': fsize,
                'buttons': buttons,
                'classes': classes,
                'ext': ext
            })

        tvars['items'] = items
        if self.makefile:
            tvars['targets'] = targets(self.makefile)
        else:
            tvars['targets'] = []

        return self.template.render(tvars).encode("utf-8")

class MakeDirectoryListingFactory (object):

    def __init__(self, makefile, template, docroot):
        self.makefile = makefile
        tpath, tname = os.path.split(template)
        templates = FileSystemLoader(tpath)
        env = Environment(loader=templates)
        env.filters['strftime'] = strftime
        env.filters['humanize_bytes'] = humanize_bytes
        self.template = env.get_template(tname)
        self.docroot = docroot

    def for_path(self, path):
        return MakeDirectoryListing(self.makefile, self.template, self.docroot, path)


