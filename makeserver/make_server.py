from __future__ import print_function
import sys, os, subprocess, time, datetime, argparse, socket
from thread import start_new_thread
from Queue import Queue
from time import sleep
from urlparse import urlparse, parse_qs
import urllib


##########################
# AUTHENTICATION
############################
# cache()
from zope.interface import implementer
from twisted.cred.portal import IRealm, Portal
from twisted.cred.checkers import FilePasswordDB
from twisted.web.static import File
from twisted.web.resource import IResource
from twisted.web.guard import HTTPAuthSessionWrapper, DigestCredentialFactory, BasicCredentialFactory
from makeserver.authentication import PublicHTMLRealm


from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.static import File, DirectoryLister
from twisted.internet import reactor
from twisted.web.resource import Resource, NoResource
from twisted.web.util import redirectTo
from twisted.web.twcgi import CGIDirectory, CGIScript
from twisted.internet.error import CannotListenError

from makeserver.makefile import make_question_simple
from makeserver.makedirectorylisting import MakeDirectoryListingFactory





# todo: parameterize this.... or default for text files!!
# TEXT_SERVE_BY_DEFAULT = ("html", "svg", "js", "css")
# TEXT_SERVE_BY_DEFAULT = ()

# http://stackoverflow.com/questions/898669/how-can-i-detect-if-a-file-is-binary-non-text-in-python
textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
def is_binary_file (p):
    """ returns none on ioerror """
    try:
        return not os.path.isdir(p) and is_binary_string(open(p, 'rb').read(1024))
    except IOError:
        return None
def is_text_file (p):
    """ returns none on ioerror """
    if os.path.isfile(p):
        try:
            return not is_binary_string(open(p, 'rb').read(1024))
        except IOError:
            return None

def parse_qs2(x):
    """ like urlparse parse_qs ... but deals with valueless keys, ie
    /path/to?foo => {'foo': None}
    /path?foo&bar=2 => {'foo': None, 'bar': 2}
    """
    ret = {}
    for p in x.split("&"):
        try:
            k, v = p.split("=")
        except ValueError:
            k = p
            v = None
        if not k: continue
        if k in ret:
            if type(ret[k] != list):
                ret[k] = [ret[k]]
            ret[k].append(v)
        else:
            ret[k] = v
    return ret

# Maker thread
def maker (path, queue):
    while True:
        if not queue.empty():
            req = queue.get()

            try:
                # _, filename = os.path.split(req['path'])
                if 'target' in req:
                    rpath = req['path']
                else:
                    rpath = os.path.relpath(req['path'], req['cwd'])
                cmd = ["make"]
                cmd.extend(("-f", req['makefile']))
                if 'force' in req:
                    cmd.append('-B')
                cmd.append(rpath)
                print ("Make {0} using {1} in {2}".format(rpath, req['makefile'], req['cwd']))
                output = subprocess.check_output(cmd, cwd=req['cwd'], stderr=subprocess.STDOUT)

                # Check if output now exists, if so redirect to it
                # If not (phony target?) Just show the output
                if 'target' in req:
                    rurl = "/"+os.path.relpath(req['cwd'], path)+"?lastbuild={0}".format(datetime.datetime.now().isoformat())
                    print ("Make successful (target). Redirecting to {0}".format(rurl))
                    redirect_body = redirectTo(rurl, req['request'])
                    req['request'].write(redirect_body)
                    req['request'].finish()
                elif os.path.exists(os.path.join(req['cwd'], req['path'])):
                    rurl = "/"+os.path.relpath(req['path'], path)+"?lastbuild={0}".format(datetime.datetime.now().isoformat())
                    print ("Make successful. Redirecting to {0}".format(rurl))
                    redirect_body = redirectTo(rurl, req['request'])
                    req['request'].write(redirect_body)
                    req['request'].finish()
                else:
                    print ("Make successful but no target file produced (phony?). Returning output.")
                    req['request'].write("<h1>{0} completed</h1>\n\n<pre class=\"error\">{1}</pre>".format(rpath, output))
                    req['request'].finish()

            except subprocess.CalledProcessError as e:
                # if make returns a non-zero value, CalledProcessError is triggered (as per subprocess.check_output)
                req['request'].setResponseCode(404)
                req['request'].write("<h1>make failed ({0})</h1>\n\n<pre class=\"error\">{1}</pre>".format(e.returncode, e.output))
                print ("Make failed ({0})\n{1}".format(e.returncode, e.output))
                req['request'].finish()

        else:
            time.sleep(0.25)

# def make_question (path):
#     """ make --question return codes: 0 : file is up to date, 1 : file needs remaking, 2 : file is not makeable """
#     return subprocess.call(["make", "--question", path])


class FileSaver (Resource):
    def __init__(self, path, docroot=None):
        self.path = path
        self.isLeaf = True
        self.docroot = docroot

    def render_POST(self, request):
        # print ("FileSaver.POST", self.path, request)
        data = request.content.read()
        data = parse_qs(data)
        # print ("data", data)
        if "touch" in data:
            # touch the file
            with open(self.path, 'a'):
                os.utime(self.path, None)
        elif "rename" in data:
            if "to" in data:
                base = os.path.split(self.path.rstrip('/'))[0]
                newpath = os.path.abspath(os.path.join(base, data['to'][0]))
                if self.docroot and not newpath.startswith(self.docroot):
                    print (u"BAD Rename attempt '{0}' to '{1}'".format(self.path, newpath).encode("utf-8"))
                    return "BAD PATH"
                # print (u"Renaming '{0}' to '{1}'".format(self.path, newpath).encode("utf-8"))
                os.rename(self.path, newpath)
        elif "text" in data:
            text = data['text'][0]
            with open(self.path, "w") as f:
                f.write(text)
        elif "data" in data:
            data = data['text'][0]
            with open(self.path, "wb") as f:
                f.write(data)
        elif "delete" in data:
            # rm the file
            # print (u"Deleting {0}".format(self.path).encode("utf-8"))
            os.remove(self.path)
        return "OK"

        # if os.path.isdir(self.path) and not self.path.endswith("/"):
        #     # redirect to self.path + "/"
        #     rpath = os.path.relpath(self.path, self.docroot)
        #     rurl = "/"+rpath+"/"
        #     # print ("rurl: '{0}'".format(rurl), file=sys.stderr)
        #     return redirectTo(rurl, request)
        # self.makeRequestsQueue.put({'path': self.path, 'request': request, 'makefile': self.makefile, 'cwd': self.docroot})
        # return NOT_DONE_YET

class PreFileMaker (Resource):
    def __init__(self, path, force=False):
        self.path = path
        self.isLeaf = True
        self.force = force

    def render_GET(self, request):
        # print ("FileSaver.POST", self.path, request)
        redurl = self.path
        if self.force:
            redurl += "?force"
        return """<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="0; url={0}">
</head>
<body>
<img src="/__makeserver__/icons/underconstruction.gif" /> Making {1}...
</body>
</html>
""".format(redurl, self.path.lstrip("/"))

class FileMaker (Resource):
    def __init__(self, path, makeRequestsQueue, docroot, index="index.html", makefile=None, force=False, target=False):
        # print ("FileMaker", path, docroot, makefile)
        self.path = path
        self.makeRequestsQueue = makeRequestsQueue
        self.docroot = docroot
        self.makefile = makefile
        self.index = index
        self.force = force
        self.target = target
        # self.cwd = cwd
        Resource.__init__(self)
        self.childNotFound = NoResource("File not found.")

    def getChild(self, path, request):
        # print ("getChild, self.path: '{0}', path:'{1}', request.path:{2}, request.postpath:{3}".format(self.path, path, request.path, request.postpath), file=sys.stderr)
        """
        getChild gets called iteratively on each part of the request path
        """

        # auto_index = False
        # if path:
        #     cpath = os.path.join(self.path, path)
        # else:
        #     # when path is empty, request is of the form "foo/" and self.path is the directory path
        #     cpath = os.path.join(self.path, "index.html")
        #     auto_index=True

        fpath = os.path.join(self.docroot, urllib.unquote(request.path.lstrip("/")))
        query = parse_qs2(urlparse(request.uri).query)

        if request.method == "POST":
            return FileSaver(fpath, self.docroot)
        if request.path == '/' or request.path.endswith("/"):
            if 'target' in query:
                # HANDLE a ?target=NAME request
                ret = FileMaker(query['target'], self.makeRequestsQueue, fpath, self.index, self.makefile, force=False, target=True)
                ret.isLeaf = True
                return ret
            else:            
                return self.index.for_path(fpath)

        # print ("fpath:{0}".format(fpath), file=sys.stderr)
        r = None
        # HOW TO AVOID THIS IF NOT NEEDED ?! >>>> ACTUALLY... or simply don't repeat again below!! (todo)
        if self.makefile:
            r = make_question_simple(fpath, self.makefile, self.docroot)
            force = 'remake' in query
            if r == 1 or force:
                if 'preview' in query:
                    # Give a WAIT PAGE
                    return PreFileMaker(request.path, force in query)
                ret = FileMaker(fpath, self.makeRequestsQueue, self.docroot, self.index, self.makefile, force=force)
                ret.isLeaf = True
                return ret
        # print ("FILE", request.path, request.uri, request.method, dir(request), request.getAllHeaders())
        # print ("FILE", request.path, request.uri, request.method)
        xRequestedWith = request.getHeader("x-requested-with")
        referer = request.getHeader('referer')
        # use_editor = False
        # base = os.path.basename(request.path)
        base = os.path.basename(fpath)
        basenoext, ext = os.path.splitext(base)
        basenoext = basenoext.lower()
        ext = ext.lstrip('.').lower()
        # print ("FILE '{0}'/'{1}'".format(base, ext))
        use_editor = "edit" in query
        if use_editor:
            return EDITOR
        else:
            # print ("returning File", fpath)
            ret = File(fpath)
            ret.isLeaf = True
            return ret
            
    def render_GET(self, request):
        print ("GET", self.path, request)
        # queue make request for self.path, and give request
        # return "<html><body>make: {0}</body></html>".format(self.path)
        # print("render_GET, self.path:'{0}'".format(self.path), file=sys.stderr)
        if os.path.isdir(self.path) and not self.path.endswith("/"):
            # redirect to self.path + "/"
            rpath = os.path.relpath(self.path, self.docroot)
            rurl = "/"+rpath+"/"
            # print ("rurl: '{0}'".format(rurl), file=sys.stderr)
            return redirectTo(rurl, request)
        d = {'path': self.path, 'request': request, 'makefile': self.makefile, 'cwd': self.docroot}
        if self.force:
            d['force'] = True
        if self.target:
            d['target'] = True
        self.makeRequestsQueue.put(d)
        return NOT_DONE_YET

    def getChildForRequest(self, request):
        print ("getChildForRequest", self.path, request)
        return super(FileMaker, self).getChildForRequest(request)

    def render_POST(self, request):
        """ is this code reached ???? """
        print ("POST", self.path, request)
        # queue make request for self.path, and give request
        # return "<html><body>make: {0}</body></html>".format(self.path)
        # print("render_GET, self.path:'{0}'".format(self.path), file=sys.stderr)
        if os.path.isdir(self.path) and not self.path.endswith("/"):
            # redirect to self.path + "/"
            rpath = os.path.relpath(self.path, self.docroot)
            rurl = "/"+rpath+"/"
            # print ("rurl: '{0}'".format(rurl), file=sys.stderr)
            return redirectTo(rurl, request)
        self.makeRequestsQueue.put({'path': self.path, 'request': request, 'makefile': self.makefile, 'cwd': self.docroot})
        return NOT_DONE_YET

EDITOR = None

def main (args=None):
    from makeserver import getDataPath
    global EDITOR

    parser = argparse.ArgumentParser(description='Happy to serve you')
    parser.add_argument('--path', default=".", help='docroot, default: .')
    parser.add_argument('--port', type=int, default=8000, help='the port number to listen to')
    parser.add_argument('-t', '--notryports', default=True, action="store_false", help='if a port is busy, automatically try other ones')
    parser.add_argument('--private', default=False, action="store_true", help='Run as server only accessible via localhost')
    # parser.add_argument('--htdocs', default="~/cookbook/", help='')
    parser.add_argument('--cgibin', default="~/cgi-bin", help='')
    parser.add_argument('--index', default=None, help='default: index.cgi')
    parser.add_argument('--makefile', default=None, help='location of the makefile to use, default ./makefile')
    parser.add_argument('--alias', nargs="+", action="append", help='Add a folder to serve, syntax is [mountpoint path] ... ')
    parser.add_argument('--authentication', default=None, help='')
    args = parser.parse_args(args)
    
    tryports = args.notryports
    port = args.port
    ipaddr = None

    # if args.share:
    #     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #     s.connect(("wikipedia.org",80))
    #     ipaddr = s.getsockname()[0]
    #     interface = ipaddr # None would listen to all
    #     s.close()
    # else:
    #     interface = "localhost"

    if args.private:
        interface = "localhost"
    else:
        interface = "0.0.0.0"

    # cwd = os.getcwd()

    docroot = os.path.abspath(args.path)
    make_requests = Queue()
    start_new_thread(maker, (docroot, make_requests))

    corehtdocspath = getDataPath("htdocs")
    corecgibinpath = getDataPath("cgi-bin")


    makefile = args.makefile
    if makefile == None:
        makefile = os.path.join(docroot, "makefile")
        if not os.path.exists(makefile):
            makefile = os.path.join(docroot, "Makefile")
            if not os.path.exists(makefile):
                makefile = None
                # raise Exception("Makefile not found")

    EDITOR = File(os.path.join(corehtdocspath, "editor/editor.html"))
    EDITOR.isLeaf = True # wow, this was subtle to find! .... fixes weird bug for requests that have subpaths (where returning the EDITOR would then (indirectly) trigger a 404 as subpaths were attempted to explore)

    if args.index:
        listing = args.index
    else:
        # listing = CGIScript(os.path.join(corecgibinpath, "index.cgi"))
        listing = MakeDirectoryListingFactory(makefile, os.path.join(getDataPath("templates"), "index.html"), docroot)

    root_resource = FileMaker(docroot, make_requests, docroot, listing, makefile)
    corehtdocs = File(corehtdocspath)
    root_resource.putChild("__makeserver__", corehtdocs)
    # corecgibin = CGIDirectory(corecgibinpath)
    # corehtdocs.putChild("cgi-bin", corecgibin)

    if args.alias:
        for mnt, path in args.alias:
        # for i in range(0, len(args.alias), 2):
        #     mnt, path = args.alias[i], args.alias[i+1]
            path = os.path.abspath(os.path.expanduser(path))
            print (u"Alias: {0} ==> {1}".format(mnt, path).encode("utf-8"))
            root_resource.putChild(mnt, File(path))

    if args.cgibin:
        root_resource.putChild("cgi-bin", CGIDirectory(args.cgibin))

    if args.authentication:
        print ("USING AUTHENTICATION")
        passwd_file = args.authentication
        portal = Portal(PublicHTMLRealm(root_resource), [FilePasswordDB(passwd_file)])
        # credentialFactory = DigestCredentialFactory("md5", "makeserver")
        credentialFactory = BasicCredentialFactory("makeserver")
        auth_root_resource = HTTPAuthSessionWrapper(portal, [credentialFactory])
        factory = Site(auth_root_resource)
    else:
        factory = Site(root_resource)

    while True:
        try:
            if ipaddr:
                server_address = (ipaddr, port)
                servername = ipaddr
            else:
                server_address = ("", port)
                servername = "localhost"

            reactor.listenTCP(port, factory, interface=interface)
            print ("Archiving starts now --> http://{0}:{1}".format(servername, port))
            reactor.run()

        except (socket.error, CannotListenError) as e:
            if isinstance(e, CannotListenError) or e.errno == 98:
                if tryports:
                    if port < 2000:
                        port = 2000
                    else:
                        port += 1
                    sleep(.01)
                else:
                    print ("""
====================================
Error: port ({0}) is already in use
====================================

You can pick another port number
(for example 9999) with:

    aa --port 9999
""").format(port)
                    break
            else:
                raise(e)


if __name__ == "__main__":


    # root = FileMaker(".")
    # factory = Site(root)
    # reactor.listenTCP(8000, factory)
    # reactor.run()

    main()
